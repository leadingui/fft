// stdafx.cpp : source file that includes just the standard includes
// LeadingUI.Algorithm.Fourier.pch will be the pre-compiled header
// stdafx.obj will contain the pre-compiled type information

#include "stdafx.h"
#include <strsafe.h>

// TODO: reference any additional headers you need in STDAFX.H
// and not in this file

#define _TRACE_BUF_SIZE		1024

void __cdecl _DbgPrintf(LPCTSTR str, ...)
{
	TCHAR buff[_TRACE_BUF_SIZE];
	va_list ap;
	va_start(ap, str);
	StringCbVPrintf(buff, sizeof buff, str, ap);
	va_end(ap);
	OutputDebugString(buff);
}
