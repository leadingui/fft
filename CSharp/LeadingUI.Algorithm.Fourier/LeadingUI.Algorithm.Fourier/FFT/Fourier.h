#pragma once

class CFourier
{
private:
	double pi;
	float *vector;

public:
	unsigned long fundamental_frequency;
	float* magnitude;

	unsigned long mSampleCounts;
	unsigned int mSample_rate;

public:
	CFourier(void);
	~CFourier(void);

	// FFT 1D
	BOOL ComplexFFT(float data[], unsigned long number_of_samples, unsigned int sample_rate, int sign);

	//With magnitude
	BOOL ComplexFFT2(float data[], unsigned long number_of_samples);
};
