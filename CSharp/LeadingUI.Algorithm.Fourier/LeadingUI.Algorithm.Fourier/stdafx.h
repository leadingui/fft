// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
// Windows Header Files:
#include <windows.h>

void __cdecl _DbgPrintf(LPCTSTR str, ...);

#ifdef _DEBUG
#define DBG _DbgPrintf
#else
#define DBG
#endif