
#pragma once

#ifdef __cplusplus
extern "C" {
#endif


#ifdef LEADINGUIALGORITHMFOURIER_EXPORTS
#define LEADINGUIALGORITHMFOURIER_API __declspec(dllexport)
#else
#define LEADINGUIALGORITHMFOURIER_API __declspec(dllimport)
#endif

/*
[API Calling Sequence]
Fourier_Init
	Fourier_complexFFT
	Fourier_GetFundamentalFrequency
	Fourier_FFTMagnitudes
Fourier_UnInit

*/

LEADINGUIALGORITHMFOURIER_API void WINAPI Fourier_Init();
LEADINGUIALGORITHMFOURIER_API void WINAPI Fourier_UnInit();
LEADINGUIALGORITHMFOURIER_API bool WINAPI Fourier_ComplexFFT(float* data, unsigned int number_of_samples);
LEADINGUIALGORITHMFOURIER_API unsigned long WINAPI Fourier_GetFundamentalFrequency();
LEADINGUIALGORITHMFOURIER_API float* WINAPI Fourier_FFTMagnitudes(unsigned int* samples);

#ifdef __cplusplus
}
#endif