#include "stdafx.h"
#include "LeadingUI.Algorithm.Fourier.h"
#include "FFT\Fourier.h"

CFourier* fourier = NULL;

LEADINGUIALGORITHMFOURIER_API void WINAPI Fourier_Init()
{
	if (fourier == NULL)
	{
		fourier = new CFourier();
	}
}

LEADINGUIALGORITHMFOURIER_API void WINAPI Fourier_UnInit()
{
	if (fourier != NULL)
	{
		delete fourier;
		fourier = NULL;
	}
}

//LEADINGUIALGORITHMFOURIER_API bool WINAPI Fourier_ComplexFFT(float* data, unsigned int number_of_samples, unsigned int sample_rate, int sign)
//{
//	if (fourier == NULL) return FALSE;
//	bool bRet = fourier->ComplexFFT(data, number_of_samples, sample_rate, sign);
//	return bRet;
//}

LEADINGUIALGORITHMFOURIER_API bool WINAPI Fourier_ComplexFFT(float* data, unsigned int number_of_samples)
{
	if (fourier == NULL) return FALSE;
	bool bRet = fourier->ComplexFFT2(data, number_of_samples);
	return bRet;
}

LEADINGUIALGORITHMFOURIER_API unsigned long WINAPI Fourier_GetFundamentalFrequency()
{
	if (fourier == NULL) return 0;
	return fourier->fundamental_frequency;
}

LEADINGUIALGORITHMFOURIER_API float* WINAPI Fourier_FFTMagnitudes(unsigned int* samples)
{
	if (fourier == NULL) return false;

	//DBG("\nVector : ");
	//for (int i = 0; i < fourier->mSample_rate*2; i++)
	//{
	//	DBG("%d : %f \n", i, fourier->vector[i]);
	//}
	//DBG("\n");
	*samples = fourier->mSample_rate / 2;
	return fourier->magnitude;
}
