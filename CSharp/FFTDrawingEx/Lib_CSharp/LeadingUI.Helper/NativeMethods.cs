﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security;
using System.Text;

namespace LeadingUI.Helper
{
	// Notes:
	// WARNING
	//	using the the x64 only signtaures on an x86 machine will result in a PInvoke stack imbalance. 
	//	For x86 and x64 platform compatibility make sure to use a signature which specifies 
	//	the Cdecl calling convention and uses the UIntPtr type to correctly marshall the size_t count argument.

	[SuppressUnmanagedCodeSecurity]
	public static class NativeMethods
	{
		//[StructLayout(LayoutKind.Sequential, Pack = 1)]
		//public struct GUID{
		//	UInt32 Data1;
		//	UInt16 Data2;
		//	UInt16 Data3;
		//	[MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
		//	byte[] Data4;

		//	public GUID(UInt32 data1, UInt16 data2, UInt16 data3, byte[] data4)
		//	{
		//		Data1 = 0;
		//		Data2 = 0;
		//		Data3 = 0;
		//		Data4 = new byte[8];
		//		Array.Copy(data4, Data4, 8);
		//	}
		//}

		#region USER32 APIs

		public const int HWND_BROADCAST = 0xffff;
		public const int MF_BYPOSITION = 0x400;
		public const int WM_SYSKEYDOWN = 0x0104;
		public const int VK_F4 = 0x73;


		public enum ShowWindowCommands : int
		{
			/// <summary>
			/// Hides the window and activates another window.
			/// </summary>
			Hide = 0,
			/// <summary>
			/// Activates and displays a window. If the window is minimized or 
			/// maximized, the system restores it to its original size and position.
			/// An application should specify this flag when displaying the window 
			/// for the first time.
			/// </summary>
			Normal = 1,
			/// <summary>
			/// Activates the window and displays it as a minimized window.
			/// </summary>
			ShowMinimized = 2,
			/// <summary>
			/// Maximizes the specified window.
			/// </summary>
			Maximize = 3, // is this the right value?
			/// <summary>
			/// Activates the window and displays it as a maximized window.
			/// </summary>       
			ShowMaximized = 3,
			/// <summary>
			/// Displays a window in its most recent size and position. This value 
			/// is similar to <see cref="Win32.ShowWindowCommand.Normal"/>, except 
			/// the window is not activated.
			/// </summary>
			ShowNoActivate = 4,
			/// <summary>
			/// Activates the window and displays it in its current size and position. 
			/// </summary>
			Show = 5,
			/// <summary>
			/// Minimizes the specified window and activates the next top-level 
			/// window in the Z order.
			/// </summary>
			Minimize = 6,
			/// <summary>
			/// Displays the window as a minimized window. This value is similar to
			/// <see cref="Win32.ShowWindowCommand.ShowMinimized"/>, except the 
			/// window is not activated.
			/// </summary>
			ShowMinNoActive = 7,
			/// <summary>
			/// Displays the window in its current size and position. This value is 
			/// similar to <see cref="Win32.ShowWindowCommand.Show"/>, except the 
			/// window is not activated.
			/// </summary>
			ShowNA = 8,
			/// <summary>
			/// Activates and displays the window. If the window is minimized or 
			/// maximized, the system restores it to its original size and position. 
			/// An application should specify this flag when restoring a minimized window.
			/// </summary>
			Restore = 9,
			/// <summary>
			/// Sets the show state based on the SW_* value specified in the 
			/// STARTUPINFO structure passed to the CreateProcess function by the 
			/// program that started the application.
			/// </summary>
			ShowDefault = 10,
			/// <summary>
			///  <b>Windows 2000/XP:</b> Minimizes a window, even if the thread 
			/// that owns the window is not responding. This flag should only be 
			/// used when minimizing windows from a different thread.
			/// </summary>
			ForceMinimize = 11
		}

		// Find window by Caption only. Note you must pass IntPtr.Zero as the first parameter.
		[DllImport("user32.dll")]
		public static extern IntPtr GetForegroundWindow();

		[DllImport("user32.dll")]
		public static extern bool SetForegroundWindow(IntPtr hWnd);

		[DllImport("user32.dll", EntryPoint = "FindWindow", SetLastError = true)]
		public static extern IntPtr FindWindowByCaption(IntPtr ZeroOnly, string lpWindowName);

		// You can also call FindWindow(default(string), lpWindowName) or FindWindow((string)null, lpWindowName)
		[DllImport("user32.dll", SetLastError = true)]
		public static extern IntPtr FindWindow(string lpClassName, string lpWindowName);

		[DllImport("user32.dll")]
		public static extern bool ShowWindow(IntPtr hwnd, ShowWindowCommands nCmdShow);

		[DllImport("user32.dll")]
		public static extern bool EnableWindow(IntPtr hwnd, bool enabled);

		[DllImport("user32.dll")]
		public static extern Int32 GetWindowThreadProcessId(IntPtr hWnd, out uint lpdwProcessId);
		
		[StructLayout(LayoutKind.Sequential)]
		public struct Win32Point
		{
			public Int32 X;
			public Int32 Y;
		};

		[DllImport("user32.dll", EntryPoint = "SetCursorPos")]
		public static extern int SetCursorPos(int x, int y);

		[DllImport("user32.dll", EntryPoint = "GetCursorPos")]
		[return: MarshalAs(UnmanagedType.Bool)]
		public static extern bool GetCursorPos(ref Win32Point pt);



#if (false)
		private static void GiveSpecifiedAppTheFocus(int processID)
		{
			try
			{
				Process p = Process.GetProcessById(processID);

				ShowWindow(p.MainWindowHandle, 1);
				SetWindowPos(p.MainWindowHandle, new IntPtr(-1), 0, 0, 0, 0, 3);

				//SetForegroundWindow(p.MainWindowHandle);
			}
			catch
			{
				throw;
			}
		}

		public bool IsForeground()
		{
			Window window = Application.Current.MainWindow;
			IntPtr windowHandle = new WindowInteropHelper(window).Handle;
			IntPtr foregroundWindow = GetForegroundWindow();
			return windowHandle == foregroundWindow;
		}

		// Returns the name of the process owning the foreground window.
		private string GetForegroundProcessName()
		{
			IntPtr hwnd = GetForegroundWindow();

			// The foreground window can be NULL in certain circumstances, 
			// such as when a window is losing activation.
			if (hwnd == null)
				return "Unknown";

			uint pid;
			GetWindowThreadProcessId(hwnd, out pid);

			foreach (Process p in Process.GetProcesses())
			{
				if (p.Id == pid)
					return p.ProcessName;
			}

			return "Unknown";
		}
#endif

		#endregion

		#region SHELL32

		[DllImport("Shell32.dll")]
		public static extern int ShellExecute(int hwnd, string lpOperation, string lpFile, string lpParameters, string lpDirectory, int nShowcmd);

		#endregion

		#region MSVCRT APIs

		[DllImport("msvcrt.dll", CallingConvention = CallingConvention.Cdecl)]
		static extern int memcmp(byte[] b1, byte[] b2, UIntPtr count);

		public static bool CompareByteArray(byte[] b1, byte[] b2)
		{
			if (b1 == b2)
				return true; //reference equality check

			if (b1 == null || b2 == null || b1.Length != b2.Length)
				return false;

			return memcmp(b1, b2, new UIntPtr((uint)b1.Length)) == 0;
		}
		
		#endregion

		#region Kernal APIs

		public const uint ERROR_SUCCESS		= 0;
		public const uint ERROR_IO_PENDING	= 997;
		public const uint WAIT_OBJECT_0		= 0;
		public const uint WAIT_TIMEOUT		= 0x00000102;
		public const uint INFINITE			= 0xFFFFFFFF;
		public const uint PROCESS_ALL_ACCESS = 0x1F0FFF;

		public const Int32 FILE_ATTRIBUTE_NORMAL	= 0X80;
		public const Int32 FILE_FLAG_OVERLAPPED		= 0X40000000;
		public const Int32 FILE_SHARE_READ			= 1;
		public const Int32 FILE_SHARE_WRITE			= 2;
		public const Int32 OPEN_EXISTING			= 3;
		public const UInt32 GENERIC_READ			= 0X80000000;
		public const UInt32 GENERIC_WRITE			= 0X40000000;

		public static readonly IntPtr INVALID_HANDLE_VALUE = new IntPtr(-1);

		//
		//  3/07/2008 Modified to handle either 32 or 64 bit platforms.
		//
		[StructLayout(LayoutKind.Sequential, Pack = 1)]
		public struct OVERLAPPED
		{
			public IntPtr Internal;
			public IntPtr InternalHigh;
			public uint   UnionPointerOffsetLow;
			public uint   UnionPointerOffsetHigh;
			public IntPtr hEvent;
		}

		[DllImport("Kernel32.dll")]
		public static extern IntPtr CreateEvent(
			[In] uint lpEventAttributes,
			[In] uint bManualReset,
			[In] uint bInitialState,
			[In] uint lpName);

		[DllImport("Kernel32.dll")]
		public static extern IntPtr CreateEvent(
			IntPtr lpEventAttributes,
			bool bManualReset,
			bool bInitialState,
			string lpName);

		[DllImport("kernel32.dll")]
		unsafe extern public static void CopyMemory(byte[] target, byte* Src, int length);

		[DllImport("kernel32.dll")]
		public static extern void CopyMemory(IntPtr Destination,// pointer to address of copy destination
											 IntPtr Source,		// pointer to address of block to copy
											 Int32 Length);		// size, in bytes, of block to copy
											 

		[DllImport("kernel32.dll", SetLastError = true)]
		public static extern Int32 WaitForSingleObject(
			[In] IntPtr h,
			[In] uint milliseconds);

		[DllImport("kernel32.dll", SetLastError = true)]
		public static extern bool GetOverlappedResult(
			[In] IntPtr h,
			[In] byte[] lpOverlapped,
			[In, Out] ref uint bytesXferred,
			[In] uint bWait);

		[DllImport("kernel32.dll")]
		public static extern IntPtr OpenProcess(UInt32 dwDesiredAccess, Int32 bInheritHandle, UInt32 dwProcessId);

		//[DllImport("kernel32.dll")]
		//public static extern Int32 CloseHandle(IntPtr hObject);
		[DllImport("kernel32.dll", SetLastError = true)]
		public static extern bool CloseHandle([In] IntPtr hDevice);


		[DllImport("KERNEL32.DLL", EntryPoint = "GetPrivateProfileStringW", 
								   SetLastError = true,
								   CharSet = CharSet.Unicode, ExactSpelling = true,
								   CallingConvention = CallingConvention.StdCall)]
		public static extern int GetPrivateProfileString( string lpAppName,
														  string lpKeyName,
														  string lpDefault,
														  string lpReturnString,
														  int nSize,
														  string lpFilename);
		[DllImport("KERNEL32.DLL", EntryPoint = "WritePrivateProfileStringW",
								   SetLastError = true,
								   CharSet = CharSet.Unicode, ExactSpelling = true,
								   CallingConvention = CallingConvention.StdCall)]
		public static extern int WritePrivateProfileString( string lpAppName, 
															string lpKeyName,
															string lpString,
															string lpFilename);

		[DllImport("KERNEL32.DLL")]
		public static extern int GetPrivateProfileInt(string lpAppName,
													  string lpKeyName,
													  int nDefault,
													  string lpFilename);


		[DllImport("kernel32.dll", EntryPoint = "LoadLibrary")]
		public static extern int LoadLibrary([MarshalAs(UnmanagedType.LPStr)] string lpLibFileName);

		[DllImport("kernel32.dll", EntryPoint = "GetProcAddress")]
		public static extern IntPtr GetProcAddress(int hModule, [MarshalAs(UnmanagedType.LPStr)] string lpProcName);

		[DllImport("kernel32.dll", EntryPoint = "FreeLibrary")]
		public static extern bool FreeLibrary(int hModule);

		[DllImport("Kernel32.dll")]
		public static extern bool QueryPerformanceCounter(out long lpPerformanceCount);

		[DllImport("Kernel32.dll")]
		public static extern bool QueryPerformanceFrequency(out long lpFrequency);


		#endregion
	}
}
