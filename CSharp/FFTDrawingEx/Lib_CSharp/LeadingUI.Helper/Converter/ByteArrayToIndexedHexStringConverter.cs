﻿
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows.Data;

namespace LeadingUI.Helper.Converter
{
    /// <summary>/// Converts a byte array to a multi-row hex string containing
    /// 
    /// byte offset indices as a prefix for each row.
    /// </summary>
    /// <example>
    /// <![CDATA[
    /// 0000:   13 31 DC 81 95 2C 92 E5
    /// 0008:   CE 14 F6 C7 2E CA 8F 13
    /// 0010:   11 2E AB 80 2E 19 63 D1
    /// 0018:   0D D6 88 2D 95 BF 03 FE
    /// 0020:   6F 8F 2C 8A D8 A9 60 B4
    /// ]]>
    /// </example>

    /*
            string strHex = "", strDispay = "";

            strHex = BitConverter.ToString(buffer).Replace('-', ' ');

            int numBytesPerRow = 8;

            int offset = 0, size = 0, chunkSize = 0;

            chunkSize = numBytesPerRow * 3; // hexstring(2byte) + blank
            
            List<String> chunks = new List<string>();
            while (offset < strHex.Length)
            {
                size = Math.Min(chunkSize, strHex.Length - offset);
                chunks.Add(strDispay = strHex.Substring(offset, size).Trim());
                offset += size;
            }

            int byteAddress = 0;
            StringBuilder sb = new StringBuilder();
            for (int i = 0;i < chunks.Count;i++)
            {
                sb.AppendLine(byteAddress.ToString("X4") + ":\t" + chunks[i]);
                byteAddress += numBytesPerRow;                
            }
    */

    public enum ConvertStyles
    {
        HexEditSytle,
        SourceStyle
    }

    [ValueConversion(typeof(byte[]), typeof(String))]
    public class ByteArrayToIndexedHexStringConverter : IValueConverter
    {
        /// <summary>    
        /// Converts a byte array to a multi-row hex string.      
        /// Each row is prefixed with the starting byte offset and contains    
        /// a fixed number of bytes.    
        /// </summary>   
        /// 
        /// <param name="value">The byte array to convert.</param>    
        /// <param name="targetType"></param>    
        /// <param name="parameter">A string, parsable to an integer, indicating    
        /// how many bytes per row.  Typically 8 or 16.</param>    
        /// <param name="culture"></param>    
        /// <returns>A string that looks similar to the HexEdit plugin for Notepad++.</returns>    
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
                return null;

            if (!(value is byte[]))
            {
                throw new ArgumentException("'value' must be a byte array.");
            }
            int numBytesPerRow;

            if ((parameter == null)
                || !(parameter is string)                
                || !(int.TryParse((string)parameter, out numBytesPerRow)))
            {
                throw new ArgumentException("'parameter' must be a string parsable to an integer (numBytesPerRow).");
            }

            //var hexSplit = BitConverter.ToString((byte[])value).Replace('-', ' ').Trim()
            //    .SplitIntoChunks(numBytesPerRow * 3)
            //    .ToList();

            string strHex = BitConverter.ToString((byte[])value).Replace('-', ' ').Trim();
            var hexSplit = SplitIntoChunks(strHex, numBytesPerRow);

            int byteAddress = 0;
            var sb = new StringBuilder();
            for (int i = 0; i < hexSplit.Count; i++)
            {
                sb.AppendLine(byteAddress.ToString("X4") + ":\t" + hexSplit[i]);
                byteAddress += numBytesPerRow;
            }
            return sb.ToString();
        }

        /// <summary>
        /// Converts the specified value.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="numBytesPerRow">The num bytes per row.</param>
        /// <returns></returns>
        public string Convert(byte[] value, int numBytesPerRow, ConvertStyles style, int StartAddress)
        {
            if (value == null)
                return null;

            if (numBytesPerRow <= 0)
            {
                throw new ArgumentException("'value' is zoro or under.");
            }

            string strHex = "";
            var hexSplit = SplitIntoChunks(strHex, numBytesPerRow * 3);
            int byteAddress = StartAddress;

            if (style == ConvertStyles.HexEditSytle)
            {
                strHex = BitConverter.ToString(value).Replace('-', ' ');
                // chunk size : "XX " -> 3
                hexSplit = SplitIntoChunks(strHex, numBytesPerRow * 3);
            }
            else if (style == ConvertStyles.SourceStyle)
            {
                // Source는 '0x'를 값 앞에 붙여 줘야한다.
                strHex = BitConverter.ToString(value).Replace("-", ",0x");
                // 변환후 처음에 0x를 추가해줘야한다.
                strHex = "0x" + strHex;

                // chunk size : "0xXX," -> 5
                hexSplit = SplitIntoChunks(strHex, numBytesPerRow * 5);
            }
            
            // Hex address 자릿수 계산
            //int hexDigit = ((int)(value.Length / 255) * 2) + 2;
			int hexDigit = value.Length.ToString("X").Length;

            string strDigit = String.Format("X{0}", hexDigit);

            var sb = new StringBuilder();
			int first_line_charters = hexSplit[0].Length;
			int last_line_charters = hexSplit[hexSplit.Count - 1].Length;

            for (int i = 0;i < hexSplit.Count;i++)
            {
                if (style == ConvertStyles.HexEditSytle)
                {
                    sb.AppendLine(byteAddress.ToString(strDigit) + ":\t" + hexSplit[i]);
                }
                else if (style == ConvertStyles.SourceStyle)
                {
					if (i == (hexSplit.Count - 1)) // 마지막 라인 정렬을 위해 Blank을 추가한다.
					{
						string strSpace = "".PadRight(first_line_charters - last_line_charters, ' ');
						sb.AppendLine("\t" + hexSplit[i] + strSpace + " \t /* 0x" + byteAddress.ToString(strDigit) + " */");
					}
					else
						sb.AppendLine("\t" + hexSplit[i] + "\t /* 0x" + byteAddress.ToString(strDigit) + " */");
                }
                byteAddress += numBytesPerRow;
            }
            return sb.ToString();
        }

        /// <summary>
        /// Converts a value.
        /// </summary>
        /// <param name="value">The value that is produced by the binding target.</param>
        /// <param name="targetType">The type to convert to.</param>
        /// <param name="parameter">The converter parameter to use.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        /// <returns>
        /// A converted value. If the method returns null, the valid null value is used.
        /// </returns>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotSupportedException();
        }

        /// <summary>
        /// Splits the into chunks.
        /// </summary>
        /// <param name="text">The text.</param>
        /// <param name="chunkSize">Size of the chunk.</param>
        /// <returns></returns>
        private IList<string> SplitIntoChunks(string text, int chunkSize)
        {
            List<string> chunks = new List<string>();
            int offset = 0;

            while (offset < text.Length)
            {
                int size = Math.Min(chunkSize, text.Length - offset);
                chunks.Add(text.Substring(offset, size));
                offset += size;
            }
            return chunks;
        }
    }

}
