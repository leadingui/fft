﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Metadata.W3cXsd2001;
using System.Text;

namespace LeadingUI.Helper.Converter
{
	public class ByteArrayToHexString
	{
		// https://bitbucket.org/patridge/convertbytearraytohexstringperftest
		// Byte Manipulation
		//    Text: 90,465 (12.06X faster)
		//    Sentence: 4.3 (14.16X faster)
		// BitConverter
		//    Text: 162,098 (6.73X faster)
		//    Sentence: 10 (6.09X faster)
		// {SoapHexBinary}.ToString
		//    Text: 338,721.1 (3.22X faster)
		//    Sentence: 18.2 (3.35X faster)
		// {byte}.ToString("X2") (using foreach)
		//    Text: 380,966 (2.86X faster)
		//    Sentence: 20.1 (3.03X faster)
		// {byte}.ToString("X2") (using {IEnumerable}.Aggregate, requires System.Linq)
		//    Text: 470,690 (2.32X faster)
		//    Sentence: 23.1 (2.64X faster)
		// Array.ConvertAll (using string.Join)
		//    Text: 952,553 (1.15X faster)
		//    Sentence: 31 (1.96X faster)
		// Array.ConvertAll (using string.Concat, requires .NET 4.0)
		//    Text: 974,442 (1.12X faster)
		//    Sentence: 24.7 (2.47X faster)
		// {StringBuilder}.AppendFormat (using foreach)
		//    Text: 999,868 (1.09X faster)
		//    Sentence: 53.2 (1.14X faster)
		// {StringBuilder}.AppendFormat (using {IEnumerable}.Aggregate, requires System.Linq)
		//    Text: 1,091,130 (1X)
		//    Sentence: 60.9 (1X)

		public static string ByteArrayToHexViaByteManipulation(byte[] bytes)
		{
			char[] c = new char[bytes.Length * 2];
			byte b;
			for (int i = 0; i < bytes.Length; i++)
			{
				b = ((byte)(bytes[i] >> 4));
				c[i * 2] = (char)(b > 9 ? b + 0x37 : b + 0x30);
				b = ((byte)(bytes[i] & 0xF));
				c[i * 2 + 1] = (char)(b > 9 ? b + 0x37 : b + 0x30);
			}
			return new string(c);
		}

		public static string ByteArrayToHexViaSoapHexBinary(byte[] bytes)
		{
			SoapHexBinary soapHexBinary = new SoapHexBinary(bytes);
			return soapHexBinary.ToString();
		}

		public static byte[] HexStringToByteArrayViaSoapHexBinary(string value)
		{
			SoapHexBinary shb = SoapHexBinary.Parse(value);
			return shb.Value;
		}

		public static string ByteArrayToHexStringViaStringJoinArrayConvertAll(byte[] bytes)
		{
			return string.Join(string.Empty, Array.ConvertAll(bytes, b => b.ToString("X2")));
		}

		public static string ByteArrayToHexStringViaStringConcatArrayConvertAll(byte[] bytes)
		{
			return string.Concat(Array.ConvertAll(bytes, b => b.ToString("X2")));
		}

		public static string ByteArrayToHexStringViaBitConverter(byte[] bytes)
		{
			string hex = BitConverter.ToString(bytes);
			return hex.Replace("-", "");
		}

		public static string ByteArrayToHexStringViaStringBuilderAggregateByteToString(byte[] bytes)
		{
			return bytes.Aggregate(new StringBuilder(bytes.Length * 2), (sb, b) => sb.Append(b.ToString("X2"))).ToString();
		}

		public static string ByteArrayToHexStringViaStringBuilderForEachByteToString(byte[] bytes)
		{
			StringBuilder hex = new StringBuilder(bytes.Length * 2);
			foreach (byte b in bytes)
				hex.Append(b.ToString("X2"));
			return hex.ToString();
		}

		public static string ByteArrayToHexStringViaStringBuilderAggregateAppendFormat(byte[] bytes)
		{
			return bytes.Aggregate(new StringBuilder(bytes.Length * 2), (sb, b) => sb.AppendFormat("{0:X2}", b)).ToString();
		}

		public static string ByteArrayToHexStringViaStringBuilderForEachAppendFormat(byte[] bytes)
		{
			StringBuilder hex = new StringBuilder(bytes.Length * 2);
			foreach (byte b in bytes)
				hex.AppendFormat("{0:X2}", b);
			return hex.ToString();
		}
	}
}
