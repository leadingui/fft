﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace LeadingUI.Helper.Converter
{
	public class ConvertHexString
	{
		public static bool IsHexInString(string test)
		{
			// For C-style hex notation (0xFF) you can use @"\A\b(0[xX])?[0-9a-fA-F]+\b\Z"
			return System.Text.RegularExpressions.Regex.IsMatch(test, @"[a-fA-F]");
		}

		public static byte[] ToByte(String strHexVal)
		{
			strHexVal = strHexVal.Trim();

			while (true)
			{
				int pos = strHexVal.IndexOf("0x", StringComparison.OrdinalIgnoreCase);

				if (pos >= 0)
				{
					strHexVal = strHexVal.Remove(pos, 2);
				}
				else
				{
					break;
				}
			}
			int NumberChars = strHexVal.Length;

			// 짝수가 아니면 에러이다. Hex는 2개의 문자로 구성되기 때문.
			if ((NumberChars % 2) != 0)
			{
				Debug.WriteLine("hex string counter error.");
				return null;
			}

			byte[] bytes = new byte[NumberChars / 2];
			for (int i = 0; i < NumberChars; i += 2)
				bytes[i / 2] = Convert.ToByte(strHexVal.Substring(i, 2), 16);
			return bytes;
		}

		public static UInt16[] ToUInt16(String strHexVal)
		{
			strHexVal = strHexVal.Trim();
			while (true)
			{
				int pos = strHexVal.IndexOf("0x", StringComparison.OrdinalIgnoreCase);

				if (pos >= 0)
				{
					strHexVal = strHexVal.Remove(pos, 2);
				}
				else
				{
					break;
				}
			}

			int NumberChars = strHexVal.Length;

			// 짝수가 아니면 에러이다. Hex는 4개의 문자로 구성되기 때문.
			if ((NumberChars % 4) != 0)
			{
				Debug.WriteLine("hex string counter error.");
				return null;
			}

			UInt16[] values = new UInt16[NumberChars / 4];

			for (int i = 0; i < NumberChars; i += 4)
				values[i / 4] = Convert.ToUInt16(strHexVal.Substring(i, 4), 16);

			return values;
		}

		public static UInt32[] ToUInt32(String strHexVal)
		{
			strHexVal = strHexVal.Trim();
			while (true)
			{
				int pos = strHexVal.IndexOf("0x", StringComparison.OrdinalIgnoreCase);

				if (pos >= 0)
				{
					strHexVal = strHexVal.Remove(pos, 2);
				}
				else
				{
					break;
				}
			}

			int NumberChars = strHexVal.Length;

			// 짝수가 아니면 에러이다. Hex는 4개의 문자로 구성되기 때문.
			if ((NumberChars % 8) != 0)
			{
				Debug.WriteLine("hex string counter error.");
				return null;
			}

			UInt32[] values = new UInt32[NumberChars / 8];
			for (int i = 0; i < NumberChars; i += 4)
				values[i / 8] = Convert.ToUInt32(strHexVal.Substring(i, 8), 16);
			return values;
		}

	}

}
