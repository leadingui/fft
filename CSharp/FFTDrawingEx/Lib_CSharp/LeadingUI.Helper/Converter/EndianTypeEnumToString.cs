﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows.Data;
using LeadingUI.Common;

namespace LeadingUI.Helper.Converter
{
	public class EndianTypeEnumToString : IValueConverter
	{
		#region IValueConverter Members

		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			if (string.IsNullOrEmpty(value.ToString()))  // This is for databinding
				return EndianType.Little;
			return (StringToEnum<EndianType>(value.ToString())).GetDescription(); // <-- The extention method
		}
		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			if (string.IsNullOrEmpty(value.ToString())) // This is for databinding
				return EndianType.Little;
			return StringToEnum<EndianType>(value.ToString());
		}

		public static T StringToEnum<T>(string name)
		{
			return (T)Enum.Parse(typeof(T), name);
		}

		#endregion
	}
}
