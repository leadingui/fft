﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;

namespace LeadingUI.Helper.Converter
{

	public sealed class AllowAllVersionDeserializationBinder : SerializationBinder
	{
		public override Type BindToType(string assemblyName, string typeName)
		{
			if (!string.IsNullOrEmpty(assemblyName) && !string.IsNullOrEmpty(typeName))
			{
				Type typeToDeserialize = null;


				assemblyName = Assembly.GetExecutingAssembly().FullName;


				// The following line of code returns the type.
				typeToDeserialize = Type.GetType(String.Format("{0}, {1}", typeName, assemblyName));


				return typeToDeserialize;
			}


			return null;
		}
	} 

	public class ObjectConvert
	{
		public static object ByteArrayToObject(byte[] byteArray)
		{
			try
			{
				// convert byte array to memory stream
				MemoryStream memoryStream = new System.IO.MemoryStream(byteArray);

				// create new BinaryFormatter
				BinaryFormatter binaryFormatter = new BinaryFormatter();
				binaryFormatter.AssemblyFormat = FormatterAssemblyStyle.Simple;
				binaryFormatter.Binder = new AllowAllVersionDeserializationBinder();

				// set memory stream position to starting point
				memoryStream.Position = 0;
				// Deserializes a stream into an object graph and return as a object.
				return binaryFormatter.Deserialize(memoryStream);
			}
			catch (Exception e)
			{
				// Error
				//Console.WriteLine("Exception caught in process: {0}", e.ToString());

				//ErrorMessage = string.Format("Exception caught in process: {0}", e.Message);
			}

			// Error occured, return null
			return null;
		}

		public static byte[] ObjectToByteArray(object obj)
		{
			try
			{
				// create new memory stream
				MemoryStream memoryStream = new MemoryStream();

				// create new BinaryFormatter
				BinaryFormatter binaryFormatter = new BinaryFormatter();
				binaryFormatter.AssemblyFormat = FormatterAssemblyStyle.Simple;
				//binaryFormatter.Binder = new AllowAllVersionDeserializationBinder();

				// Serializes an object, or graph of connected objects, to the given stream.
				binaryFormatter.Serialize(memoryStream, obj);
				// convert stream to byte array and return
				return memoryStream.ToArray();
			}
			catch (Exception e)
			{
				// Error
				//Console.WriteLine("Exception caught in process: {0}", e.ToString());
				//ErrorMessage = string.Format("Exception caught in process: {0}", e.Message);
			}

			// Error occured, return null
			return null;
		}
	}
}
