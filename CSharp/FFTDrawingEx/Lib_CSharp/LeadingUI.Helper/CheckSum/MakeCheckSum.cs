﻿using LeadingUI.Common;
using LeadingUI.Helper.Converter;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Windows;

namespace LeadingUI.Helper
{
	public class Checksum
	{
		/// <summary>
		/// Calculate the checksum of the file.
		/// </summary>
		/// <param name="strFilePath">The STR file path.</param>
		/// <param name="dataBytes">The data bytes.</param>
		/// <param name="endianType">Type of the endian.</param>
		/// <returns>UInt32.</returns>
		public static UInt32 MakeFromFile(string strFilePath, int dataBytes, EndianType endianType = EndianType.Little)
		{
			UInt32 checkSum = 0;

			byte[] fileBuff = null;
			int nReadBytes = 0;

			try
			{
				using (FileStream fStream = new FileStream(strFilePath,
												FileMode.Open,
												FileAccess.Read,
												FileShare.None))
				{
					nReadBytes  = (int)fStream.Length;
					fileBuff = new byte[nReadBytes];

					nReadBytes = fStream.Read(fileBuff, 0, nReadBytes);
					//fStream.Close();
				}
			}
			catch (Exception e)
			{
				Debug.WriteLine("MakeFromFile Error:{0}", e.Message);
				return 0;
			}

			if (fileBuff == null)
			{
				Debug.WriteLine("Read file error");
				return 0;
			}

			//Make Checksum
			checkSum = MakeFromBuffer(fileBuff, dataBytes, 0, endianType);

			Debug.WriteLine("0x{0:X8}", (UInt32)checkSum);

			if (endianType == EndianType.Little)
			{
				return checkSum;
			}
			else
			{
				//return ConCatenating.MakeWord(ConCatenating.HiByte(checkSum),
				//							  ConCatenating.LoByte(checkSum));
				return Endian.SwapUInt32(checkSum);
			}
		}

		public static UInt32 MakeFromBuffer(byte[] buffer, int checksumBytes, int index = 0, EndianType endianType = EndianType.Little)
		{
			UInt32 checkSum = 0;

			byte[] checkBuff = buffer;

			// remainder is zero?
			if (((buffer.Length - index) % checksumBytes) != 0)
			{
				//throw new ArgumentException("buffer size error");
				Debug.WriteLine("MakeFromBuffer: buffer size / checksum bytes is not even-numbered.");

				int orgLength = (buffer.Length - index);

				int remainByte = orgLength % checksumBytes;
				int totalLength = orgLength + (checksumBytes - remainByte); // 모자란 바이트를 추가로 잡아주기 위해 전체 크기를 변경한다.

				byte[] newBuff = new byte[totalLength];

				Array.Copy(buffer, index, newBuff, 0, orgLength);

				if (checksumBytes == 1)
				{
					for (int i = 0; i < totalLength; i++)
					{
						checkSum += newBuff[i];
					}
				}
				else if (checksumBytes == 2)
				{
					for (int i = 0; i < totalLength; i += checksumBytes)
					{
						checkSum += BitConverter.ToUInt16(newBuff, i);
					}
				}
				else if (checksumBytes == 4)
				{
					for (int i = 0; i < totalLength; i += checksumBytes)
					{
						checkSum += BitConverter.ToUInt32(newBuff, i);
					}
				}
				else
				{
					throw new ArgumentException("Unknown Data Bytes");
				}

			}
			else
			{
				if (checksumBytes == 1)
				{
					for (int i = index; i < buffer.Length; i++)
					{
						checkSum += buffer[i];
					}
				}
				else if (checksumBytes == 2)
				{
					for (int i = index; i < buffer.Length; i += checksumBytes)
					{
						checkSum += BitConverter.ToUInt16(buffer, i);
					}
				}
				else if (checksumBytes == 4)
				{
					for (int i = index; i < buffer.Length; i += checksumBytes)
					{
						checkSum += BitConverter.ToUInt32(buffer, i);
					}
				}
				else
				{
					throw new ArgumentException("Unknown Data Bytes");
				}
			}

			if (endianType == EndianType.Little)
			{
				return checkSum;
			}
			else
			{
				return Endian.SwapUInt32(checkSum);
			}
		}

		public static UInt32 MakeFromBuffer2(byte[] buffer, int index, int checksumBytes, EndianType endianType = EndianType.Little)
		{
			return MakeFromBuffer2(buffer, index, buffer.Length, checksumBytes, endianType);
		}

		/// <summary>
		/// Calculate the checksum of the buffer.
		/// </summary>
		/// <param name="buffer">The buffer. The Array that contains the data to checksum. </param>
		/// <param name="index">The index. A 32-bit integer that represents the index in the sourceArray at which checksum begins.</param>
		/// <param name="length">The length. A 32-bit integer that represents the number of elements to checksum. </param>
		/// <param name="checksumBytes">The checksum bytes.</param>
		/// <param name="endianType">Type of the endian.</param>
		/// <returns>UInt32.</returns>
		/// <exception cref="System.ArgumentNullException">buffer is null</exception>
		/// <exception cref="System.ArgumentException">
		/// length > buffer size
		/// or
		/// Unknown Data Bytes
		/// or
		/// Unknown Data Bytes
		/// </exception>
		/// <exception cref="System.ArgumentOutOfRangeException">offset + length > buffer size</exception>
		public static UInt32 MakeFromBuffer2(byte[] buffer, int index, int length, int checksumBytes, EndianType endianType = EndianType.Little)
		{
			if (buffer == null)
			{
				throw new ArgumentNullException("buffer is null");
			}

			if (buffer.Length < length)
			{
				throw new ArgumentException("length > buffer size");
			}

			if (buffer.Length < (index + length))
			{
				throw new ArgumentOutOfRangeException("offset + length > buffer size");
			}

			UInt32 checkSum = 0;
			byte[] checkBuff = buffer;

			// remainder is zero?
			if ((length % checksumBytes) != 0)
			{
				//throw new ArgumentException("buffer size error");
				Debug.WriteLine("MakeFromBuffer: buffer size / checksum bytes is not even-numbered.");

				int orgLength = length;

				int remainByte = orgLength % checksumBytes;
				int totalLength = orgLength + (checksumBytes - remainByte); // 모자란 바이트를 추가로 잡아주기 위해 전체 크기를 변경한다.

				byte[] newBuff = new byte[totalLength];

				Array.Copy(buffer, index, newBuff, 0, orgLength);

				if (checksumBytes == 1)
				{
					for (int i = 0; i < totalLength; i++)
					{
						checkSum += newBuff[i];
					}
				}
				else if (checksumBytes == 2)
				{
					for (int i = 0; i < totalLength; i += checksumBytes)
					{
						checkSum += BitConverter.ToUInt16(newBuff, i);
					}
				}
				else if (checksumBytes == 4)
				{
					for (int i = 0; i < totalLength; i += checksumBytes)
					{
						checkSum += BitConverter.ToUInt32(newBuff, i);
					}
				}
				else
				{
					throw new ArgumentException("Unknown Data Bytes");
				}

			}
			else
			{
				if (checksumBytes == 1)
				{
					for (int i = index; i < length; i++)
					{
						checkSum += buffer[i];
					}
				}
				else if (checksumBytes == 2)
				{
					for (int i = index; i < length; i += checksumBytes)
					{
						checkSum += BitConverter.ToUInt16(buffer, i);
					}
				}
				else if (checksumBytes == 4)
				{
					for (int i = index; i < length; i += checksumBytes)
					{
						checkSum += BitConverter.ToUInt32(buffer, i);
					}
				}
				else
				{
					throw new ArgumentException("Unknown Data Bytes");
				}
			}

			if (endianType == EndianType.Little)
			{
				return checkSum;
			}
			else
			{
				return Endian.SwapUInt32(checkSum);
			}
		}

	}
}
