﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LeadingUI.Helper
{
	public class IniFile
	{
		public static List<string> GetCategories(string iniFile)
		{
			string returnString = new string(' ', 65536);
			NativeMethods.GetPrivateProfileString(null, null, null, returnString, 65536, iniFile);
			List<string> result = new List<string>(returnString.Split('\0'));
			result.RemoveRange(result.Count - 2, 2);
			return result;
		}

		public static List<string> GetKeys(string iniFile, string category)
		{
			string returnString = new string(' ', 32768);
			NativeMethods.GetPrivateProfileString(category, null, null, returnString, 32768, iniFile);
			List<string> result = new List<string>(returnString.Split('\0'));
			result.RemoveRange(result.Count-2,2);
			return result;
		}

		public static string GetIniFileString(string iniFile, string category, string key, string defaultValue)
		{
			string returnString = new string(' ', 1024);
			NativeMethods.GetPrivateProfileString(category, key, defaultValue, returnString, 1024, iniFile);
			return returnString.Split('\0')[0];
		}

		public void IniWriteValue(string iniFile, string Section, string Key, string Value)
		{
			NativeMethods.WritePrivateProfileString(Section, Key, Value, iniFile);
		}

		public static int GetIniFileInt(string iniFile, string category, string key, int defaultValue)
		{
			return NativeMethods.GetPrivateProfileInt(category, key, defaultValue, iniFile);
		}

		public static bool WriteIniFileString(string iniFile, string Section, string Key, string Value)
		{
			if (NativeMethods.WritePrivateProfileString(Section, Key, Value, iniFile) > 0)
				return true;
			else
				return false;
		}
	}
}
