﻿using System;
using System.ComponentModel;
using System.Threading;
//using System.Runtime.InteropServices;

namespace LeadingUI.Helper.Timer
{
	public class HiPerforTimer
	{
		private long startTime, stopTime;
		private long frequency;
		Decimal multiplier = new Decimal(1.0e9);

		private bool _isStarting = false;

		// Constructor
		public HiPerforTimer()
		{
			startTime = 0;
			stopTime  = 0;

			if (NativeMethods.QueryPerformanceFrequency(out frequency) == false)
			{
				// high-performance counter not supported
				throw new Win32Exception();
			}
		}

		// Start the timer
		public void Start()
		{
			// lets do the waiting threads there work
			//Thread.Sleep(0);

			NativeMethods.QueryPerformanceCounter(out startTime);
			stopTime = startTime;

			this.IsRunning = true;
		}

		// Stop the timer
		public void Stop()
		{
			NativeMethods.QueryPerformanceCounter(out stopTime);

			this.IsRunning = false;
		}

		public bool IsRunning
		{
			get { return _isStarting; }
			internal set { _isStarting = value; }
		}

		// StartTime은 Timer가 Stop된 상태에서만 변경할 수 있다.
		public long StartTime
		{
			get { return startTime; }
			set 
			{
				if (this.IsRunning)
					return;

				startTime = value; 
			}
		}

		// Returns the duration of the timer (in seconds)
		//public double Duration()
		//{
		//	return (double)(stopTime - startTime) / (double)frequency;
		//}

		// Timer가 동작중일때에는 시작부터 현재까지의 시간을 반환하며
		// Timer가 정지 되었을때에는 Start부터 Stop한 시점까지의 시간을 반환한다.
		public double ElapsedMilliseconds
		{
			get 
			{
				if (this.IsRunning)
				{
					NativeMethods.QueryPerformanceCounter(out stopTime);
				}

				return (Duration(1)/1000000);
			}
		}

		// Calculate time per iteration in nanoseconds
		public double Duration(int iterations)
		{
			return ((((double)(stopTime - startTime) * (double)multiplier) / (double)frequency) / iterations);
		}
	}

// ******************************************
//         Example
// ******************************************
//
//	public static void RunTest()
//	{
//		int iterations=5;
//
//		// Call the object and methods to JIT before the test run
//		HiPerforTimer myTimer = new HiPerforTimer();
//		myTimer.Start();
//		myTimer.Stop();
//
//		// Time the overall test duration
//		DateTime dtStartTime = DateTime.Now;
//
//		// Use QueryPerfCounters to get the average time per iteration
//		myTimer.Start();
//
//		for(int i = 0; i < iterations; i++)
//		{
//		  // Method to time
//		  System.Threading.Thread.Sleep(1000);
//		}
//		myTimer.Stop();
//
//		// Calculate time per iteration in nanoseconds
//		double result = myTimer.Duration(iterations);
//
//		// Show the average time per iteration results
//		Console.WriteLine("Iterations: {0}", iterations);
//		Console.WriteLine("Average time per iteration: ");
//		Console.WriteLine(result/1000000000 + " seconds");
//		Console.WriteLine(result/1000000 + " milliseconds");
//		Console.WriteLine(result + " nanoseconds");
//
//		// Show the overall test duration results
//		DateTime dtEndTime = DateTime.Now;
//		Double duration = ((TimeSpan)(dtEndTime-dtStartTime)).TotalMilliseconds;
//		Console.WriteLine();
//		Console.WriteLine("Duration of test run: ");
//		Console.WriteLine(duration/1000 + " seconds");
//		Console.WriteLine(duration + " milliseconds");
//		Console.ReadLine();
//
//		// Iterations: 5
//		// Average time per iteration:
//		// 0.999648279320416 seconds
//		// 999.648279320416 milliseconds
//		// 999648279.320416 nanoseconds
//
//		// Duration of test run:
//		// 5.137792 seconds
//		// 5137.792 milliseconds
//	}

}
