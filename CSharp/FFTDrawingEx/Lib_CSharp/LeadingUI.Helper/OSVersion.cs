﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LeadingUI.Helper
{
	public class OSVersion
	{
		/// <summary>
		/// Gets a Version that identifies the operating system.
		/// </summary>
		/// <returns></returns>
		public static Version GetOsVersion()
		{
			OperatingSystem os = Environment.OSVersion;

			return os.Version;

			//switch (os.Platform)
			//{
			//case PlatformID.Win32Windows:
			//	{
			//		//The operating system is Windows 95 or Windows 98.
			//		if (os.Version.Major == 4)
			//		{
			//			switch (os.Version.Minor)
			//			{
			//			case 0: // Windows 95
			//			case 1: // Windows 98
			//			case 2: // Windows Me
			//				break;
			//			}
			//		}
			//	}
			//	break;
			//case PlatformID.Win32NT:
			//	{
			//		if (os.Version.Major == 4)
			//		{
			//			// Windows NT 4.0
			//		}
			//		else if (os.Version.Major == 5)
			//		{
			//			switch (os.Version.Minor)
			//			{
			//			case 0: // Windows 2000
			//				break;
			//			case 1: // Windows XP
			//				break;
			//			case 2: // Windows XP 64-Bit, Windows Server 2003, 2003 R2

			//				break;
			//			}
			//		}
			//		else if (os.Version.Major == 6)
			//		{
			//			switch (os.Version.Minor)
			//			{
			//			case 0: // Windows Vista, Windows Server 2008
			//				break;
			//			case 1: // Windows 7, Windows Server 2008 R2
			//				break;
			//			case 2: // Windows 8
			//				break;
			//			}
			//		}
			//	}
			//	break;
			//}
		}

		/// <summary>
		/// Determines whether [is windows XP].
		/// </summary>
		/// <returns>
		///   <c>true</c> if [is windows XP]; otherwise, <c>false</c>.
		/// </returns>
		public static bool IsWindowsXP()
		{
			bool bResult = false;

			OperatingSystem os = Environment.OSVersion;

			switch (os.Platform)
			{
			case PlatformID.Win32NT:
				{
					if (os.Version.Major == 5)
					{
						switch (os.Version.Minor)
						{
						case 0: // Windows 2000
							break;
						case 1: // Windows XP
							bResult = true;
							break;
						case 2: // Windows XP 64-Bit, Windows Server 2003, 2003 R2							
							break;
						}
					}
				}
				break;
			}

			return bResult;
		}

		/// <summary>
		/// Determines whether [is windows vista].
		/// </summary>
		/// <returns>
		///   <c>true</c> if [is windows vista]; otherwise, <c>false</c>.
		/// </returns>
		public static bool IsWindowsVista()
		{
			bool bResult = false;

			OperatingSystem os = Environment.OSVersion;

			switch (os.Platform)
			{
			case PlatformID.Win32NT:
				{
					if (os.Version.Major == 6)
					{
						switch (os.Version.Minor)
						{
						case 0: // Windows Vista, Windows Server 2008
							bResult = true;
							break;
						case 1: // Windows 7, Windows Server 2008 R2
							break;
						case 2: // Windows 8
							break;
						}
					}
				}
				break;
			}

			return bResult;
		}

		/// <summary>
		/// Determines whether this instance is windows 7.
		/// </summary>
		/// <returns>
		///   <c>true</c> if this instance is windows7; otherwise, <c>false</c>.
		/// </returns>
		public static bool IsWindows7()
		{
			bool bResult = false;

			OperatingSystem os = Environment.OSVersion;

			switch (os.Platform)
			{
			case PlatformID.Win32NT:
				{
					if (os.Version.Major == 6)
					{
						switch (os.Version.Minor)
						{
						case 0: // Windows Vista, Windows Server 2008
							break;
						case 1: // Windows 7, Windows Server 2008 R2
							bResult = true;
							break;
						case 2: // Windows 8
							break;
						}
					}
				}
				break;
			}

			return bResult;
		}

		/// <summary>
		/// Determines whether this instance is windows 8.
		/// </summary>
		/// <returns>
		///   <c>true</c> if this instance is windows8; otherwise, <c>false</c>.
		/// </returns>
		public static bool IsWindows8()
		{
			bool bResult = false;

			OperatingSystem os = Environment.OSVersion;

			switch (os.Platform)
			{
			case PlatformID.Win32NT:
				{
					if (os.Version.Major == 6)
					{
						switch (os.Version.Minor)
						{
						case 0: // Windows Vista, Windows Server 2008
							break;
						case 1: // Windows 7, Windows Server 2008 R2
							break;
						case 2: // Windows 8
							bResult = true;
							break;
						}
					}
				}
				break;
			}

			return bResult;
		}

		/// <summary>
		/// Determines whether the current operating system is a 64-bit operating system.
		/// </summary>
		/// <returns></returns>
		public static bool Is64BitOS()
		{
			return Environment.Is64BitOperatingSystem;
		}

		/// <summary>
		/// Determines whether the current process is a 64-bit process.
		/// </summary>
		/// <returns></returns>
		public static bool Is64BitProcess()
		{
			return Environment.Is64BitProcess;
		}
	}
}
