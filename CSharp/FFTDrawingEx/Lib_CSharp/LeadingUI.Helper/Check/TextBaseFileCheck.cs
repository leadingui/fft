﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace LeadingUI.Helper.Check
{
	/// <summary>
	/// Class TextBaseFileCheck
	/// 이 함수는 파일이 Text Base의 파일인지 체크하는 클래스 이다.
	/// </summary>
	public class TextBaseFileCheck
	{
		private Encoding _encoding;

		#region Properties

		/// <summary>
		/// Gets the encoding.
		/// </summary>
		/// <value>The Encoding.</value>
		public Encoding Encoding
		{
			get { return _encoding; }
			internal set { _encoding = value; }
		}

		/// <summary>
		/// 
		/// </summary>
		public bool IsTextFile = false;

		#endregion

		public static bool IsTextBaseFile(string fileName)
		{
			TextBaseFileCheck check = new TextBaseFileCheck();

			return check.IsTextFile = check.IsTextBaseFile(fileName, 10);
		}

		public bool CheckTextBaseFile(string fileName)
		{
			return IsTextFile = IsTextBaseFile(fileName, 10);
		}

		private bool IsTextBaseFile(string fileName, int windowSize)
		{
			bool isText = true;

			//using (var fileStream = System.IO.File.OpenRead(fileName))
			FileStream fileStream = System.IO.File.OpenRead(fileName);
			if (fileStream == null)
				return false;

			{
				var rawData = new byte[windowSize];
				var text = new char[windowSize];

				// Read raw bytes
				var rawLength = fileStream.Read(rawData, 0, rawData.Length);
				fileStream.Seek(0, SeekOrigin.Begin);

				// Detect encoding correctly (from Rick Strahl's blog)
				// http://www.west-wind.com/weblog/posts/2007/Nov/28/Detecting-Text-Encoding-for-StreamReader
				if (rawData[0] == 0xef && rawData[1] == 0xbb && rawData[2] == 0xbf)
				{
					_encoding = Encoding.UTF8;
				}
				else if (rawData[0] == 0xfe && rawData[1] == 0xff)
				{
					_encoding = Encoding.Unicode;
				}
				else if (rawData[0] == 0 && rawData[1] == 0 && rawData[2] == 0xfe && rawData[3] == 0xff)
				{
					_encoding = Encoding.UTF32;
				}
				else if (rawData[0] == 0x2b && rawData[1] == 0x2f && rawData[2] == 0x76)
				{
					_encoding = Encoding.UTF7;
				}
				else
				{
					_encoding = Encoding.Default;
				}

				// Read text and detect the encoding
				//using (var streamReader = new StreamReader(fileStream))
				StreamReader streamReader = new StreamReader(fileStream);
				if (streamReader == null)
					return false;

				{
					streamReader.Read(text, 0, text.Length);

					//using (var memoryStream = new MemoryStream())
					MemoryStream memoryStream = new MemoryStream();
					if (memoryStream == null)
						return false;

					{
						using (var streamWriter = new StreamWriter(memoryStream, _encoding))
						//StreamWriter streamWriter = new StreamWriter(memoryStream, _encoding);
						//if (streamWriter == null)
						//	return false;

						{
							// Write the text to a buffer
							streamWriter.Write(text);
							streamWriter.Flush();

							// Get the buffer from the memory stream for comparision
							var memoryBuffer = memoryStream.GetBuffer();

							// Compare only bytes read
							for (var i = 0; i < rawLength && isText; i++)
							{
								isText = rawData[i] == memoryBuffer[i];
							}
						}
					}

					memoryStream.Dispose();
				}				
				streamReader.Dispose();
			}

			fileStream.Close();

			return isText;
		}

	}


}
