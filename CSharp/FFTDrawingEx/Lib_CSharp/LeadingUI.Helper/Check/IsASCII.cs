﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LeadingUI.Helper.Check
{
	public class ASCII
	{
		public static bool IsASCII(string value)
		{
			// ASCII encoding replaces non-ascii with question marks, so we use UTF8 to see if multi-byte sequences are there
			return Encoding.UTF8.GetByteCount(value) == value.Length;
		}
	}
}
