﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;

namespace LeadingUI.Common.Usb
{
	[StructLayout(LayoutKind.Sequential, Pack = 1)]
	public struct GUID
	{
		public UInt32 Data1;
		public UInt16 Data2;
		public UInt16 Data3;
		[MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
		public byte[] Data4;

		public GUID(UInt32 data1, UInt16 data2, UInt16 data3, byte[] data4)
		{
			Data1 = 0;
			Data2 = 0;
			Data3 = 0;
			Data4 = new byte[8];
			Array.Copy(data4, Data4, 8);
		}

		public bool Equals(GUID other)
		{
			if (other.Data4 == null)
				return false;

			if ((this.Data1 == other.Data1) && (this.Data2 == other.Data2) && (this.Data3 == other.Data3))
			{
				for (int i=0; i< 8; i++)
				{
					if (this.Data4[i] != other.Data4[i])
						return false;
				}

				return true;
			}

			return false;
		}

		public static Guid FromGUID(GUID guid)
		{
			return new Guid((int)guid.Data1, (short)guid.Data2, (short)guid.Data3, guid.Data4);
		}

		public static GUID ToGUID(Guid guid)
		{
			GUID newGuid = new GUID();
			byte[] guidData = guid.ToByteArray();
			newGuid.Data1 = BitConverter.ToUInt32(guidData, 0);
			newGuid.Data2 = BitConverter.ToUInt16(guidData, 4);
			newGuid.Data3 = BitConverter.ToUInt16(guidData, 6);
			newGuid.Data4 = new byte[8];
			Array.Copy(guidData, 8, newGuid.Data4, 0, 8);
			return newGuid;
		}

	}

	[StructLayout(LayoutKind.Sequential, Pack = 1)]
	public struct USB_ID
	{
		public ushort Vendor;
		public ushort Product;

		public USB_ID(ushort idVendor, ushort idProduct)
		{
			this.Vendor = idVendor;
			this.Product = idProduct;
		}

		public USB_ID(USB_ID usb_id)
		{
			this.Vendor = usb_id.Vendor;
			this.Product = usb_id.Product;
		}

		public bool Equals(USB_ID other)
		{
			return (this.Vendor == other.Vendor) &&
				   (this.Product == other.Product);
		}
	}

	/// <summary>
	/// 
	/// </summary>
	/// 
	[StructLayout(LayoutKind.Sequential, Pack = 1)]
	public struct DriverID
	{
		//private Guid _guid;
		private GUID	_guid;
		private USB_ID	_usbID;

		//public UsbIDS()
		//{
		//	//_guid = new Guid("{0x00000000, 0x0000, 0x0000, {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}}");
		//	_guid = ToGUID(new Guid());
		//	_usbID = new USB_ID(0x0000, 0x0000);
		//	//_idVendor = 0x00;
		//	//_idProduct = 0x00;
		//}

		public DriverID(string guid, ushort vid, ushort pid)
		{
			_guid  = GUID.ToGUID(new Guid(guid));
			_usbID = new USB_ID(vid, pid);
		}

		public DriverID(DriverID id)
		{
			_guid = id.GUID;
			_usbID = new USB_ID(id.USB_ID.Vendor, id.USB_ID.Product);
		}
				
		public GUID GUID
		{
			get { return _guid; }
			set { _guid = value; }
		}

		public Guid Guid
		{
			get { return GUID.FromGUID(_guid); }
			set { _guid = GUID.ToGUID(value); }
		}

		public string UsbGuid
		{
			get { return GUID.FromGUID(_guid).ToString(); }
			set { _guid = GUID.ToGUID(new Guid(value)); }
		}

		public ushort VID
		{
			get { return _usbID.Vendor; }
			set { _usbID.Vendor = value; }
		}

		public ushort PID
		{
			get { return _usbID.Product; }
			set { _usbID.Product = value; }
		}

		public USB_ID USB_ID
		{
			get
			{
				return _usbID;
			}
		}

		public bool Equals(DriverID other)
		{
			return (this.Guid.Equals(other.Guid)) &&
				   (VID == other.VID) &&
				   (PID == other.PID);
		}
	}
}
