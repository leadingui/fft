﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LeadingUI.Common.Usb
{
	public class MaxPacketSize
	{
		public static int XP_ControlPipes	= 1024 * 4;
		public static int XP_OtherPipies	= 1024 * 64;
	}
}
