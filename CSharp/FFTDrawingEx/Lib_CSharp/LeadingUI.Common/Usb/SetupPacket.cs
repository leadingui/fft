﻿using System;
using System.Runtime.InteropServices;

namespace LeadingUI.Common.Usb
{
    //bmRequest.Dir
    public class bmRequestDir
    {
        public const byte BMREQUEST_HOST_TO_DEVICE = 0;
        public const byte BMREQUEST_DEVICE_TO_HOST = 1;    
    }

    //bmRequest.Type
    public class bmRequestType
    {
        public const byte BMREQUEST_STANDARD = 0;
        public const byte BMREQUEST_CLASS    = 1;
        public const byte BMREQUEST_VENDOR   = 2;
    }

    //bmRequest.Recipient
    public class bmRequestRecipient
    {
        public const byte BMREQUEST_TO_DEVICE     = 0;
        public const byte BMREQUEST_TO_INTERFACE  = 1;
        public const byte BMREQUEST_TO_ENDPOINT   = 2;
        public const byte BMREQUEST_TO_OTHER      = 3;
    }

    public class RequestTypes
    {
        public const byte VendorWrite = 0x40;
        public const byte VendorRead = 0xC0;
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct SetupPacket
    {
        public byte   bmRequestType;
        public byte   bRequest;
        public ushort wValue;
        public ushort wIndex;
        public ushort wLength;
    }


    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct UsbDescriptorRequest 
    {
        public int         ConnectionIndex;
        public SetupPacket SetupPacket;
        public byte[]      Data;
    }



}
