﻿using System;
using System.Runtime.InteropServices;

namespace LeadingUI.Common
{
	[StructLayout(LayoutKind.Explicit, Size = 2, CharSet = CharSet.Ansi)]
	public class ErrorStatus
	{
		[FieldOffset(0)]
		public byte hi;
		[FieldOffset(1)]
		public byte lo;

		public ErrorStatus()
		{
			hi = lo = 0x00;
		}

		public ushort ErrorCode
		{
			get
			{
				return ConCatenating.MakeWord(lo, hi);
			}
		}
	}
}
