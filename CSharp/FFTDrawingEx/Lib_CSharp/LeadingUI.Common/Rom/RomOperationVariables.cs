﻿using LeadingUI.Common.Device;
using System;

namespace LeadingUI.Common.Rom
{

	public enum RomEraseErrCodes
	{
		NoError = 0,
		UsbBridgeIsNull,
		EraseAllCommandError,
		Verify,
		RomRWCmdError,
		RomReadCmdError,
		RomWriteCmdError,
	}

	public enum RomAreas
	{
		All,
		Program,
		Parameters,  // defatul parameters
		Calibration,
		DeviceInfoData,
	}

	public class RomEraseParams
	{
		public RomAreas   RomArea;
		public DeviceType DeviceType;
	}

	public enum RomOperateMode
	{
		Write = 0,
		Read = 1,
		Erase = 2
	}

	public class RomDownloadParams
	{
		public DeviceType     DeviceType;
		public RomOperateMode OpMode;
		public RomAreas		  RomArea;
		public string         RomPath; // hex or bin
		public string         BinPath;
		public UInt32         CheckSum;
		//public int            RomSize;
	}

}
