﻿using System;
using System.Runtime.InteropServices;

namespace LeadingUI.Common.Device
{
	[StructLayout(LayoutKind.Sequential, Pack = 1)]
	[Serializable()]
	public class Version8
	{
		private byte version = 0x00;

		public Version8()
		{
		}

		public Version8(byte ver)
		{
			version = ver;
		}

		public Version8 (byte major, byte minor)
		{
			version = (byte)(((major & 0x0F) << 4) | (minor & 0x0F));
		}

		public Version8 (Version8 version)
		{
			if (version != null)
				Byte = version.Byte;
		}

		public byte Byte
		{
			get { return version; }
			set { version = value; }
		}

		public byte Major
		{
			get { return (byte)(version >> 4); }
			set { version = (byte)((value << 4) | (version & 0x0F)); }
		}

		public byte Minor
		{
			get { return (byte)(version & 0x0F); }
			set { version = (byte)((value & 0x0F) | (version & 0xF0)); }
		}

		public override string ToString()
		{
			return string.Format("{0}.{1}", Major, Minor);
		}

		public bool CompareTo(Version8 info)
		{
			return (this.Byte == info.Byte);
		}

		public static int Size
		{
			get { return Marshal.SizeOf(typeof(Version8)); }
		}

	}

}
