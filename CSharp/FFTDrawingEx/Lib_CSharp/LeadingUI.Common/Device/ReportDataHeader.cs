﻿using System;
using System.Runtime.InteropServices;

namespace LeadingUI.Common.Device
{
	[StructLayout(LayoutKind.Sequential, Pack = 1)]
	public class ReportDataHeader
	{
		public byte Status;		// 0x0000	1	현재 Firmware 의 내부 상태를 의미한다.
								//	0x00 일 경우 정상 상태를 의미하며, 그 외의 값은 Error 가 발생된 상태를 의미한다.
		public byte EventType;	// 0x0001	1	현재 보고된 Report 의 Type 을 의미함.
								//	B0 : Absolute Point
								//	B1 : Key Event
								//	B2 : Gesture Event
								//	B3 : Relative Event
								//	B4 : Pressure Event
								//	B5 : Rsv
								//	B6 : Rsv
								//	B7 : Debug Event
		public byte TouchCount;	// 0x0003 Touch 0 ~Touch 9의 데이터 와 Pressure Data 의 유효한 개수를 알려준다 
		public byte KeyData;	// 0x0004 TSP 에 Key 가 있을 경우 Key Data 를 보고한다.
								// KeyData 0이면 Release이고 Press일때에는 1~254개의 값이 된다.
		public ReportDataHeader()
		{
			Status     = 0x00;
			EventType  = 0x00;
			TouchCount = 0x00;
			KeyData    = 0x00;
		}

		public ReportDataHeader(byte status = 0, byte eventType = 0, byte touchCount = 0, byte keyData = 0)
		{
			Status     = status;
			EventType  = eventType;
			TouchCount = touchCount;
			KeyData    = keyData;
		}

		public ReportDataHeader(ReportDataHeader prevHeader)
		{
			Status     = prevHeader.Status;
			EventType  = prevHeader.EventType;
			TouchCount = prevHeader.TouchCount;
			KeyData    = prevHeader.KeyData;
		}


		/// <summary>
		/// size of ReportDataHeader Structure
		/// </summary>
		/// <value>The length.</value>
		public static int Size
		{
			get { return Marshal.SizeOf(typeof(ReportDataHeader)); }
		}

		public bool IsAbsolutePoint
		{
			get
			{
				return ((EventType & EventBitMaskTypes.AbsolutePoint) == EventBitMaskTypes.AbsolutePoint);
			}
		}
		public bool IsKeyEvent
		{
			get
			{
				return ((EventType & EventBitMaskTypes.KeyEvent) == EventBitMaskTypes.KeyEvent);
			}
		}
		public bool IsGestureEvent
		{
			get
			{
				return ((EventType & EventBitMaskTypes.GestureEvent) == EventBitMaskTypes.GestureEvent);
			}
		}
		public bool IsRelativeEvent
		{
			get
			{
				return ((EventType & EventBitMaskTypes.RelativeEvent) == EventBitMaskTypes.RelativeEvent);
			}
		}
		public bool IsPressureEvent
		{
			get
			{
				return ((EventType & EventBitMaskTypes.PressureEvent) == EventBitMaskTypes.PressureEvent);
			}
		}
		public bool IsDebugEvent
		{
			get
			{
				return ((EventType & EventBitMaskTypes.DebugEvent) == EventBitMaskTypes.DebugEvent);
			}
		}

		public int ButtonNumber
		{
			get { return (int)KeyData; }
		}

		public byte[] GetBytes
		{
			get
			{
				byte[] buff = new byte[Marshal.SizeOf(typeof(ReportDataHeader))];

				buff[0] = Status;
				buff[1] = EventType;
				buff[2] = TouchCount;
				buff[3] = KeyData;			

				return buff;
			}
		}
	}
}
