﻿using System;
using System.ComponentModel;

namespace LeadingUI.Common.Device
{
    /// <summary>
    /// Touch Screen Types
    /// </summary>
    public enum TspType : byte
    {
        [Description("1 Layer SR")]
        SingleLayerSR = 0x00,

        [Description("1 Layer DR")]
        SingleLayerDR = 0x01,

        [Description("2 Layer SR")]
        DoubleLayerSR = 0x02,

        [Description("2 Layer DR")]
        DoubleLayerDR = 0x03
    }

    /// <summary>
    /// Touch Screen Types
    /// </summary>
    public class TspTypes
    {
        public const byte SingleLayerSR = 0x01;
        public const byte SingleLayerDR = 0x02;
        public const byte DoubleLayerSR = 0x03;
        public const byte DoubleLayerDR = 0x04;
    }
}
