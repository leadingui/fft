﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LeadingUI.Common.Device
{
	public enum ReloadDataType : ushort
	{
		All				= 0x0000,
		Parameter		= 0x0001,
		CalibrationData	= 0x0002,
		ReferenceValue	= 0x0003,
	}

	public class ReloadDataTypes
	{
		public const ushort All				= 0x0000;
		public const ushort Parameter		= 0x0001;
		public const ushort CalibrationData	= 0x0002;
		public const ushort ReferenceValue	= 0x0003;
	}
}
