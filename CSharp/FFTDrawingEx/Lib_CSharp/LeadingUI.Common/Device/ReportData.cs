﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Security;
using System.Text;

namespace LeadingUI.Common.Device
{
	/// <summary>
	/// 
	/// </summary>
	[StructLayout(LayoutKind.Sequential, Pack = 1)]
	public class ReportData
	{
		private byte			 _deviceType;
		public ReportDataHeader  _header;
		public ReportTouchData[] _touchData;
		public UInt16[]          _pressureData;

		/// <summary>
		/// Initializes a new instance of the <see cref="ReportData" /> class.
		/// </summary>
		/// <param name="header">The header.</param>
		/// <exception cref="System.ArgumentNullException">Invalid Parameter.</exception>
		public ReportData(DeviceType deviceType, ReportDataHeader header)
		{
			if (header == null)
				throw new ArgumentNullException("Invalid Parameter.");

			DeviceType = deviceType;
			Header     = new ReportDataHeader(header);
			TouchData  = new ReportTouchData[Header.TouchCount];

			for (int i = 0; i < Header.TouchCount; i++)
			{
				TouchData[i] = new ReportTouchData();
			}
						
			if ((DeviceType == DeviceType.LU3010) ||
				(DeviceType == DeviceType.LU3100) ||
				(DeviceType == DeviceType.LU310A) ||
				(DeviceType == DeviceType.LU3200))
			{
				_pressureData = new UInt16[Header.TouchCount];
				for (int i = 0; i < this.PressureData.Length; i++)
					PressureData[i] = 0;
			}
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="ReportData" /> class.
		/// </summary>
		/// <param name="header">The header.</param>
		/// <param name="touchValue">The touch value.</param>
		/// <param name="startTouchIndex">Start index of the touch.</param>
		/// <param name="pressureValue">The pressure value.</param>
		/// <param name="startPressureIndex">Start index of the pressure.</param>
		/// <exception cref="System.ArgumentNullException">Invalid Parameter.</exception>
		public ReportData ( DeviceType deviceType, 
							ReportDataHeader header, 
							byte[] touchValue,    int startTouchIndex,
							byte[] pressureValue, int startPressureIndex)
		{
			if ((header == null) || (touchValue == null))
				throw new ArgumentNullException("Invalid Parameter.");

			this.DeviceType = deviceType;
			this.Header     = new ReportDataHeader(header);
			this.TouchData  = new ReportTouchData[Header.TouchCount];

			for (int i = 0; i < Header.TouchCount; i++)
			{
				this.TouchData[i] = ReportTouchData.FromBytes(touchValue, startTouchIndex + (i * 4));
			}

			if ((this.DeviceType == DeviceType.LU3010) ||
				(this.DeviceType == DeviceType.LU3100) ||
				(this.DeviceType == DeviceType.LU310A) ||
				(this.DeviceType == DeviceType.LU3200))
			{
				PressureData = new UInt16[this.PressureData.Length];
				for (int i = 0; i < this.PressureData.Length; i++)
					PressureData[i] = 0;
			}
		}

		public static ushort GetReportDataSize(DeviceType deviceType, int MaxPoint)
		{
			if (deviceType == DeviceType.LU2010)
			{
				// Header + TouchData
				//return (ushort)(Marshal.SizeOf(typeof(ReportDataHeader)) +
				//				(Marshal.SizeOf(typeof(ReportTouchData)) * MaxPoint));
				return (ushort) (ReportDataHeader.Size +
								(ReportTouchData.Size * MaxPoint));
			}
			else if ((deviceType == DeviceType.LU3010) || 
					 (deviceType == DeviceType.LU3100) || 
					 (deviceType == DeviceType.LU310A) ||
					 (deviceType == DeviceType.LU3200))
			{
				// Header + TouchData + PressureData
				//return (ushort)(Marshal.SizeOf(typeof(ReportDataHeader)) +
				//				(Marshal.SizeOf(typeof(ReportTouchData)) * MaxPoint) +
				//				(Marshal.SizeOf(typeof(UInt16)) * MaxPoint));
				return (ushort) (ReportDataHeader.Size +
								(ReportTouchData.Size * MaxPoint) +
								(Marshal.SizeOf(typeof(UInt16)) * MaxPoint));
			}

			return 0;
		}

		public DeviceType DeviceType
		{
			get { return (DeviceType)_deviceType; }
			internal set { _deviceType = (byte)value; }
		}

		public ReportDataHeader Header
		{
			get { return _header; }
			internal set { _header = value; }
		}

		public ReportTouchData[] TouchData
		{
			get { return _touchData; }
			internal set { _touchData = value; }
		}
		
		public UInt16[] PressureData
		{
			get { return _pressureData; }
			internal set { _pressureData = value; }
		}
			 
		public ReportData ShallowCopy()
		{
			return (ReportData)this.MemberwiseClone();
		}

		public ReportData DeepCopy()
		{
			ReportData other = (ReportData)this.MemberwiseClone();

			other.TouchData = new ReportTouchData[this.TouchData.Length];
			for (int i = 0; i < this.TouchData.Length; i++)
			{
				other.TouchData[i] = new ReportTouchData(this.TouchData[i].Data);
			}

			other.PressureData = new UInt16[this.PressureData.Length];
			for (int i = 0; i < this.PressureData.Length; i++)
			{
				other.PressureData[i] = this.PressureData[i];
			}

			return other;
		}

		public void SetTouchData(byte[] value, int startIndex)
		{
			if ((value == null) || (value.Length < (Header.TouchCount * 4)))
				throw new ArgumentException("Invalid Parameter.");

			if ((startIndex < 0) || (value.Length < (startIndex + (Header.TouchCount * 4))))
				throw new ArgumentException("Invalid Parameter.");

			for (int i = 0; i < Header.TouchCount; i++)
			{
				TouchData[i].Data = BitConverter.ToUInt32(value, startIndex + (i * 4));
			}
		}

		public void SetPressureData(byte[] value, int startIndex)
		{
			if ((value == null) || (value.Length < (Header.TouchCount * 2)))
				throw new ArgumentException("Invalid Parameter.");

			if ((startIndex < 0) || (value.Length < (startIndex + (Header.TouchCount * 2))))
				throw new ArgumentException("Invalid Parameter.");

			for (int i = 0; i < Header.TouchCount; i++)
			{
				PressureData[i] = BitConverter.ToUInt16(value, startIndex + (i * 2));
			}
		}

		[SecurityCritical,SecurityTreatAsSafe]
		public byte[] GetBytes
		{
			get
			{
				//******************
				// 수정해야 함다.
				int pressureSize = Marshal.SizeOf(typeof(UInt16));

				int buffLength = 1 + Marshal.SizeOf(typeof(ReportDataHeader));

				if (Header.IsAbsolutePoint || Header.IsRelativeEvent)
				{
					buffLength += (Header.TouchCount * ReportTouchData.Size);

					if ((Header.IsPressureEvent) &&
						(DeviceType == DeviceType.LU3010 || 
						 DeviceType == DeviceType.LU3100 || 
						 DeviceType == DeviceType.LU310A ||
						 DeviceType == DeviceType.LU3200))
					{
						buffLength += (Header.TouchCount * pressureSize);
					}
				}

				int pos = 0;

				byte[] buff = new byte[buffLength];

				buff[pos] = _deviceType;
				pos += 1;

				Array.Copy(Header.GetBytes, 0, buff, pos, ReportDataHeader.Size);
				pos += ReportDataHeader.Size;

				if (Header.IsAbsolutePoint || Header.IsRelativeEvent)
				{
					for (int i=0; i<Header.TouchCount; i++)
					{
						Array.Copy(_touchData[i].GetBytes, 0, buff, pos, ReportDataHeader.Size);
						pos += ReportDataHeader.Size;
					}
					
					if ((Header.IsPressureEvent) &&
						(DeviceType == DeviceType.LU3010 || 
						 DeviceType == DeviceType.LU3100 ||
						 DeviceType == DeviceType.LU310A ||
						 DeviceType == DeviceType.LU3200))
					{
						for (int i = 0; i < Header.TouchCount; i++)
						{
							Array.Copy(BitConverter.GetBytes(_pressureData[i]), 0, buff, pos, pressureSize);
							pos += pressureSize;
						}
					}
				}

				return buff;
			}
		}
	}
}
