﻿using System;
using System.ComponentModel;

namespace LeadingUI.Common.Device
{
    /// <summary>
    /// Touch Controller Solution Types
    /// </summary>

    public enum SolutionType : byte
    {
        [Description("Single Point")]
        SinglePoint = 0x00,

        [Description("Multi Point")]
        MultiPoint = 0x01,
        
        [Description("Single Point & Gesture")]
        SingleGesture = 0x02,

        [Description("Button Only")]
        Button = 0x03,
    }

    public class SolutionTypes
    {
        public const byte SinglePoint   = 0x00;
        public const byte MultiPoint    = 0x01;
        public const byte SingleGesture = 0x02;
        public const byte Button        = 0x03;
    }
}
