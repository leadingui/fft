﻿using System;
using System.ComponentModel;

namespace LeadingUI.Common.Device
{
    /// <summary>
    /// Touch Screen Types
    /// </summary>
    public enum TpmType : byte
    {
        [Description("One Layer")]
        OneLayer = 0x01,

        [Description("Two Layer")]
        TwoLayer = 0x02,
    }

    /// <summary>
    /// Touch Screen Types
    /// </summary>
    public class TpmTypes
    {
        public const byte OneLayer = 0x01;
        public const byte TwoLayer = 0x02;
    }
}
