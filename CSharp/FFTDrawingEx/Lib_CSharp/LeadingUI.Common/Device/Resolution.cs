﻿// ***********************************************************************
// Assembly         : LeadingUI.Common
// Author           : jaehan
// Created          : 03-25-2013
//
// Last Modified By : jaehan
// Last Modified On : 03-27-2013
// ***********************************************************************
// <copyright file="Resolution.cs" company="">
//     . All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Runtime.InteropServices;

namespace LeadingUI.Common.Device
{
	/// <summary>
	/// Struct Resolution
	/// </summary>
	[StructLayout(LayoutKind.Sequential, Pack = 1)]
	[Serializable()]
	public struct Resolution
	{
		/// <summary>
		/// 
		/// </summary>
		public ushort Width;
		/// <summary>
		/// 
		/// </summary>
		public ushort Height;

		/// <summary>
		/// Initializes a new instance of the <see cref="Resolution"/> struct.
		/// </summary>
		/// <param name="width">The width.</param>
		/// <param name="height">The height.</param>
		public Resolution(ushort width, ushort height)
		{
			this.Width = width;
			this.Height = height;
		}

		/// <summary>
		/// Clones this instance.
		/// </summary>
		/// <returns>Resolution.</returns>
		public Resolution Clone()
		{
			return new Resolution(this.Width, this.Height);
		}

		/// <summary>
		/// Compares the specified resolution.
		/// </summary>
		/// <param name="resolution">The resolution.</param>
		/// <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
		public bool Compare(Resolution resolution)
		{
			if (this.Width != resolution.Width)
				return false;

			if (this.Height != resolution.Height)
				return false;

			return true;
		}

		/// <summary>
		/// size of Resolution Structure
		/// </summary>
		/// <value>The length.</value>
		public static int Size
		{
			get { return Marshal.SizeOf(typeof(Resolution)); }
		}
	}
}
