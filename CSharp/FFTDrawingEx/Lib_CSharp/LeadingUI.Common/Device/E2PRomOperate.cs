﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;

namespace LeadingUI.Common.Device
{
	public class E2PRomAccess
	{
		public const bool On = true;
		public const bool Off = false;
	}

	public class E2PRomReadWriteModes
	{
		public const bool Read = true;
		public const bool Write = false;
	}

	[StructLayout(LayoutKind.Sequential, Pack = 1)]
	public class E2PRomOperate
	{
		public bool ReadWrite = true; // true is read, false is write
		public int Address = 0;
		public int Length = 0;
		public byte[] Buffer;

		public E2PRomOperate(bool read, int address, byte[] buffer, int bufferLength)
		{
			ReadWrite = read; // true is read, false is write
			Address   = address;
			Length    = bufferLength;
			Buffer    = buffer;
		}
	}
}
