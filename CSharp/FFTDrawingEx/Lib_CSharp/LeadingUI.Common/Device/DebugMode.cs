﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace LeadingUI.Common.Device
{

	// Device 의 동작 모드를 Debug Mode 로 설정할때 사용
	// SET_DEBUG_MODE Command 사용
	//public class DebugModeType
	//{
	//	public const byte None          = (byte)OperationMode_Minor.None;
	//	public const byte AbHisto       = (byte)OperationMode_Minor.AbHisto;
	//	public const byte SumHisto      = (byte)OperationMode_Minor.SumHisto;
	//	public const byte BtnHisto      = (byte)OperationMode_Minor.BtnHisto;
	//	public const byte CurCap        = (byte)OperationMode_Minor.CurCap;
	//	public const byte RefCap        = (byte)OperationMode_Minor.RefCap;
	//	public const byte SumDivHisto   = (byte)OperationMode_Minor.SumDivHisto;
	//	public const byte SrAbHisto     = (byte)OperationMode_Minor.SrAbHisto;
	//	public const byte SrSumHisto    = (byte)OperationMode_Minor.SrSumHisto;
	//	public const byte SrBtnHisto    = (byte)OperationMode_Minor.SrBtnHisto;
	//	public const byte SrCurCap      = (byte)OperationMode_Minor.SrCurCap;  
	//	public const byte SrRefCap      = (byte)OperationMode_Minor.SrRefCap;  
	//	public const byte SrSumDivHisto = (byte)OperationMode_Minor.SrSumDivHisto;
	//	public const byte DrBtnRatio    = (byte)OperationMode_Minor.DrBtnRatio;
	//}

	//public class DebugModeTypes
	//{
	//	public const byte None        = 0x00;	
	//	public const byte ABHisto     = 0x01;	
	//	public const byte SumHisto    = 0x02;	
	//	public const byte BtnHisto    = 0x03;	
	//	public const byte SRHisto     = 0x04;	
	//	public const byte CurCap      = 0x05; // Current Cap
	//	public const byte RefCap      = 0x06; // Reference Cap
	//	public const byte SumDivHisto = 0x07; // 
	//}
}
