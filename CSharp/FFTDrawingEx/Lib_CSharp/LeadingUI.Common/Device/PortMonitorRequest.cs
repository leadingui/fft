﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LeadingUI.Common.Device
{
	public class PortMonitorRequest
	{
		//private DebugPortType _portType;
		//private bool		  _isMonitor;
		//private ushort		  _i2cAddress;
		//private ushort		  _i2cReportSize;
		//private ushort		  _readSize;

		/// <summary>
		/// Initializes a new instance of the <see cref="PortMonitorRequest"/> class.
		/// </summary>
		/// <param name="portType">Type of the port.</param>
		/// <param name="OnOff">if set to <c>true</c> [on off].</param>
		/// <param name="i2cAddress">The i2c address. only I2C Port</param>
		/// <param name="i2cReportSize">Size of the i2c report. only I2C Port</param>
		/// <param name="readsize">The usb readsize per call.</param>
		//public PortMonitorRequest(DebugPortType portType, 
		//						bool OnOff, 
		//						ushort i2cAddress, 
		//						ushort i2cReportSize,
		//						ushort usbReadsize = 512)
		//{
		//	_portType      = portType;
		//	_isMonitor     = OnOff;
		//	_i2cAddress    = i2cAddress;
		//	_i2cReportSize = i2cReportSize;
		//	_readSize      = usbReadsize;
		//}

		public PortMonitorRequest(  DebugPortType portType, 
									bool OnOff, 
									ushort i2cAddress = 0, 
									ushort i2cDataLength = 0)
		{
			DebugPortType = portType;
			IsMonitor     = OnOff;
			I2cAddress    = i2cAddress;
			I2cDataLength = i2cDataLength;

			if ((portType == Device.DebugPortType.I2C) && (OnOff == true))
			{
				if (i2cDataLength <= 0)
				{
					throw new ArgumentException("i2c port monitor data length is zero");
				}
			}
		}

		public DebugPortType DebugPortType
		{
			get; //{ return _portType; }
			private set;
		}

		public bool IsMonitor
		{
			get;// { return _isMonitor; }
			private set;
		}

		public ushort I2cAddress
		{
			get;// { return _i2cAddress; }
			private set;
		}

		//public ushort I2cReportSize
		//{
		//	//get { return _i2cReportSize; }
		//	//set { _i2cReportSize = value; }
		//	get;
		//	private set;
		//}

		public ushort I2cDataLength
		{
			get;
			private set;
		}

		//public ushort Readsize
		//{
		//	get { return _readSize; }
		//}
	}
}
