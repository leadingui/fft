﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.InteropServices;
using System.Text;

namespace LeadingUI.Common.Device
{
	public class LU2010
	{
		public static DeviceID DeviceID = new DeviceID(DeviceID.LU2010);

		public const int MaxTouchPoints = 2;

		public const int MaxChannels = 40;

		public const int ModelInfoSize = 16;

		public const int MaxCalBarCount = 5;

		public enum OperationMode_Major : byte
		{
			[Description("Normal")]
			Normal = 0x00,

			[Description("Power Down")]
			PowerDown = 0x01,

			[Description("Debug")]
			Debug = 0x02,

			[Description("Factory Calibration")]
			FactoryCalibration = 0x03,

			[Description("Position Check")]
			PositionCheck = 0x04,
		}

		public enum OperationMode_Minor : byte
		{
			// Minor Mode
			[Description("None")]
			None = 0x00,

			[Description("Absolute Histo")]
			AbHisto = 0x01,

			[Description("Sum Histo")]
			SumHisto = 0x02,

			[Description("Button Histo")]
			BtnHisto = 0x03,

			[Description("Current Cap")]
			CurCap = 0x04,

			[Description("Reference Cap")]
			RefCap = 0x05,

			[Description("Sum Divide Histo")]
			SumDivHisto = 0x06,

			[Description("SR Absolute Histo")]
			SrAbHisto = 0x07,

			[Description("SR Sum Histo")]
			SrSumHisto = 0x08,

			[Description("SR Button Histo")]
			SrBtnHisto = 0x09,

			[Description("SR Current Cap")]
			SrCurCap = 0x0A,

			[Description("SR Reference Cap")]
			SrRefCap = 0x0B,

			[Description("SR Sum Divide Histo")]
			SrSumDivHisto = 0x0C,

			[Description("DR Button Ratio")]
			DrBtnRatio = 0x0D,
		}

		//[StructLayout(LayoutKind.Sequential, Pack = 1)]
		//public class OperationMode
		//{
		//    private OperationMode_Major _major;
		//    private OperationMode_Minor _minor;

		//    public OperationMode(OperationMode_Major major, OperationMode_Minor minor)
		//    {
		//        _major = major;
		//        _minor = minor;
		//    }

		//    public OperationMode_Major Major
		//    {
		//        get { return _major; }
		//        set { _major = value; }
		//    }

		//    public OperationMode_Minor Minor
		//    {
		//        get { return _minor; }
		//        set { _minor = value; }
		//    }

		//    public byte MajorValue
		//    {
		//        get { return (byte)_major; }
		//    }

		//    public byte MinorValue
		//    {
		//        get { return (byte)_minor; }
		//    }
		//}
	}
}
