﻿using System;

namespace LeadingUI.Common.Device
{
	//EventType
	//B0 : Absolute Point
	//B1 : Key Event
	//B2 : Gesture Event
	//B3 : Relative Event
	//B4 : Pressure Event
	//B5 : Rsv
	//B6 : Rsv
	//B7 : Debug Event
	public class EventBitMaskTypes
	{
		public const byte AbsolutePoint = 0x01;
		public const byte KeyEvent		= 0x02;
		public const byte GestureEvent	= 0x04;
		public const byte RelativeEvent	= 0x08;
		public const byte PressureEvent	= 0x10;
		//public const byte Rsv			= 0x20;
		//public const byte Rsv			= 0x40;
		public const byte DebugEvent	= 0x80;
	}

	public class ReportEventType
	{

		public static bool IsAbsolutePoint(byte type)
		{
			return ((type & EventBitMaskTypes.AbsolutePoint) == EventBitMaskTypes.AbsolutePoint);
		}
		public static bool IsKeyEvent(byte type)
		{
			return ((type & EventBitMaskTypes.KeyEvent) == EventBitMaskTypes.KeyEvent);
		}
		public static bool IsGestureEvent(byte type)
		{
			return ((type & EventBitMaskTypes.GestureEvent) == EventBitMaskTypes.GestureEvent);
		}
		public static bool IsRelativeEvent(byte type)
		{
			return ((type & EventBitMaskTypes.RelativeEvent) == EventBitMaskTypes.RelativeEvent);
		}
		public static bool IsPressureEvent(byte type)
		{
			return ((type & EventBitMaskTypes.PressureEvent) == EventBitMaskTypes.PressureEvent);
		}
		public static bool IsDebugEvent(byte type)
		{
			return ((type & EventBitMaskTypes.DebugEvent) == EventBitMaskTypes.DebugEvent);
		}
	}
}
