﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.InteropServices;
using System.Text;

namespace LeadingUI.Common.Device
{
	public enum ProductGroup : byte
	{
		[Description("TSP 1DR")]
		TSP_1DR = 0x00,
		
		[Description("TSP 2DR")]
		TSP_2DR = 0x01,
		
		[Description("Remote Controller")]
		REMOCON = 0x02,
		
		[Description("Button")]
		BUTTON = 0x03,

		[Description("Mutul")]
		MUTUAL = 0x04,
	}

	public enum DeviceType : byte
	{
		//UNKNOWN = 0x00,
		[Description("LU2020")]
		LU2020 = 0x04,
		[Description("LU2010")]
		LU2010 = 0x05,
		[Description("LU3010")]
		LU3010 = 0x06,
		[Description("LU2009")]
		LU2009 = 0x07,
		[Description("LU3100")]
		LU3100 = 0x08,
		[Description("LU310A")]
		LU310A = 0x09,
		[Description("LU3200")]
		LU3200 = 0x0A,
		//[Description("UNKNOWN")]
		//UNKNOWN = 0xFF
}

	/// <summary>
	/// 
	///  ID BitMask
	/// |--------------------------------------------------------|
	///	|      Product Group	  | Product     |   Device       |
	///	|                         | Version     |	             |
	/// |--------------------------------------------------------|
	///	| D7 D6 D5 D4 D3 D2 D1 D0 | D7 D6 D5 D4 | D3 D2 D1 D0	 |
	/// |--------------------------------------------------------|
	///	|    0x00 = TSP 1DR       |             | 0x00 =         |
	///	|    0x01 = TSP 2DR		  |             | 0x01 =         |
	///	|    0x02 = Remote		  |             | 0x02 =         |
	///	|           Controller	  |             | 0x03 =         |
	///	|    0x03 = Button		  |             | 0x04 = LU2020  |
	///	|    0x04 = TSP_MUTUAL	  |             | 0x05 = LU2010	 |
	///	|    		              |             | 0x06 = LU3010	 |
	///	|    		              |             | 0x07 = LU2009	 |
	///	|    		              |             | 0x08 = LU3100	 |
	///	|    		              |             | 0x09 = LU310A	 |
	///	|    		              |             | 0x0A = LU3200	 |
	///	|    		              |             |  				 |
	///	|--------------------------------------------------------|
	///	
	/// </summary>
	[StructLayout(LayoutKind.Sequential, Pack = 1)]
	public class DeviceGroup
	{
		private ushort _id;

		public DeviceGroup()
		{
			_id = 0x0000;
		}

		public DeviceGroup(ushort id)
		{
			_id = id;
		}

		public DeviceGroup(DeviceGroup dg)
		{
			_id = dg.Id;
		}

		public DeviceGroup(ProductGroup group, DeviceType id)
		{
			this.ProductGroup = group;
			this.DeviceType = id;
		}

		public ushort Id
		{
			get { return _id; }
			set { _id = value; }
		}

		public ProductGroup ProductGroup
		{
			get
			{
				return (ProductGroup)((_id & 0xFF00) >> 8);
			}

			set
			{
				_id = (ushort)((_id & 0x00FF) | ((ushort)value << 8));
			}
		}

		public DeviceType DeviceType
		{
			get 
			{
				return (DeviceType)(_id & 0x000F);
			}
			set
			{
				_id = (ushort)((_id & 0xFFF0) | ((ushort)value & 0x000F));
			}
		}

		public DeviceID DeviceID
		{
			get
			{
				if (this.DeviceType == Device.DeviceType.LU2010)
					return new DeviceID(DeviceID.LU2010);
				else if (this.DeviceType == Device.DeviceType.LU2020)
					return new DeviceID(DeviceID.LU2020);
				else if (this.DeviceType == Device.DeviceType.LU3010)
					return new DeviceID(DeviceID.LU3010);
				else if (this.DeviceType == Device.DeviceType.LU3100)
					return new DeviceID(DeviceID.LU3100);
				else if (this.DeviceType == Device.DeviceType.LU310A)
					return new DeviceID(DeviceID.LU310A);
				else if (this.DeviceType == Device.DeviceType.LU3200)
					return new DeviceID(DeviceID.LU3200);
				else
					return new DeviceID();
			}

			set
			{
				if (value.IsLU2010)
					this.DeviceType = DeviceType.LU2010;
				else if (value.IsLU2020)
					this.DeviceType = DeviceType.LU2020;
				else if (value.IsLU3010)
					this.DeviceType = DeviceType.LU3010;
				else if (value.IsLU3100)
					this.DeviceType = DeviceType.LU3100;
				else if (value.IsLU310A)
					this.DeviceType = DeviceType.LU310A;
				else if (value.IsLU3200)
					this.DeviceType = DeviceType.LU3200;
				//else
				//	this.DeviceType = DeviceType.UNKNOWN;
			}
		}

		public byte Version
		{
			get
			{
				return (byte)((_id & 0x00F0)>>4);
			}

			set
			{
				_id = (ushort)((_id & 0xFF0F) | (((ushort)value & 0x000F)<<4));
			}
		}

		public static ushort GetDeviceGroup(ProductGroup group, DeviceType id)
		{
			DeviceGroup dg = new DeviceGroup(group, id);
			return dg.Id;
		}

		public bool Equals(DeviceGroup other)
		{
			return (Id == other.Id);
		}
		
		/// <summary>
		/// size of DeviceGroup Structure
		/// </summary>
		/// <value>The size.</value>
		//public static int Size
		//{
		//	get
		//	{
		//		return Marshal.SizeOf(typeof(DeviceGroup));
		//	}
		//}
	}


}
