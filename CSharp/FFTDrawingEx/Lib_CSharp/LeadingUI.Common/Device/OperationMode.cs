﻿using LeadingUI.Common.UsbBridge;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.InteropServices;
using System.Text;

namespace LeadingUI.Common.Device
{
#if (false)
	public enum OperationMode_Major : byte
	{
		[Description("Normal")]
		Normal             = 0x00,

		[Description("Power Down")]
		PowerDown          = 0x01,

		[Description("Debug")]
		Debug              = 0x02,

		[Description("Factory Calibration")]
		FactoryCalibration = 0x03,

		[Description("Position Check")]
		PositionCheck      = 0x04,
	}

	public enum OperationMode_Minor : byte
	{
		// Minor Mode
		[Description("None")]
		None          = 0x00,

		[Description("Absolute Histo")]
		AbHisto       = 0x01,
		
		[Description("Sum Histo")]
		SumHisto      = 0x02,
		
		[Description("Button Histo")]
		BtnHisto      = 0x03,
		
		[Description("Current Cap")]
		CurCap        = 0x04,
		
		[Description("Reference Cap")]
		RefCap        = 0x05,
		
		[Description("Sum Divide Histo")]
		SumDivHisto   = 0x06,
		
		[Description("SR Absolute Histo")]
		SrAbHisto     = 0x07,
		
		[Description("SR Sum Histo")]
		SrSumHisto    = 0x08,
		
		[Description("SR Button Histo")]
		SrBtnHisto    = 0x09,
		
		[Description("SR Current Cap")]
		SrCurCap      = 0x0A,
		
		[Description("SR Reference Cap")]
		SrRefCap      = 0x0B,
		
		[Description("SR Sum Divide Histo")]
		SrSumDivHisto = 0x0C,
		
		[Description("DR Button Ratio")]
		DrBtnRatio    = 0x0D,
	}

#endif

	[StructLayout(LayoutKind.Sequential, Pack = 1)]
	public struct OperationMode
	{
		public byte Major;
		public byte Minor;

		public OperationMode(byte major, byte minor)
		{
			Major = major;
			Minor = minor;
		}

		//public byte Major
		//{
		//	get { return _major; }
		//	set { _major = value; }
		//}

		//public byte Minor
		//{
		//	get { return _minor; }
		//	set { _minor = value; }
		//}

        //public byte MajorValue
        //{
        //    get { return (byte)_major; }
        //}

        //public byte MinorValue
        //{
        //    get { return (byte)_minor; }
        //}

		public UInt16 UInt16
		{
			get 
			{
				return (ushort)((Major << 8) | (Minor));
			}
			set
			{
				Major = (byte)(value >> 8);
				Minor = (byte)(value & 0xFF);
			}
		}
	}
}
