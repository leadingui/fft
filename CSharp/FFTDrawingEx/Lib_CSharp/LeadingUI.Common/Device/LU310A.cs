﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.InteropServices;
using System.Text;

namespace LeadingUI.Common.Device
{
	public class LU310A
	{
		public static DeviceID DeviceID	= new DeviceID(DeviceID.LU310A);

		public const int MaxTouchPoints	= 10;
		public const int ModelNameSize	= 16;
		public const int TxMaxChannels	= 30;
		public const int RxMaxChannels	= 16;
		public const int MaxNodes		= TxMaxChannels * RxMaxChannels;

		public enum OperationMode_Major : byte
		{
			[Description("Normal")]
			Normal = 0x00,

			[Description("Power Down")]
			PowerDown = 0x01,

			[Description("Debug")]
			Debug = 0x02,

			[Description("Factory Test Mode")]
			Factory = 0x03,

			[Description("Manufactory Mode")]
			Manufactory = 0x04,
		}

		public enum OperationMode_Minor_Debug : byte
		{
			// Minor Mode
			[Description("None")]
			None = 0x00,

			[Description("Dual Mode Histo")]
			//AbHisto = 0x01,
			DualModeHisto = 0x01,

			[Description("Common Mode Histo")]
			CommonModeHisto = 0x02,

			[Description("Differential Mode Histo")]
			DifferentialModeHisto = 0x03,

			[Description("Dual Mode Cap")]
			DualModeCap = 0x04,

			[Description("Common Mode Cap")]
			CommonModeCap = 0x05,

			[Description("Differential Mode Cap")]
			DifferentialModeCap = 0x06,

			[Description("Reference Cap")]
			ReferenceCap = 0x07,
		}

		public enum OperationMode_Minor_Factory : byte
		{
			// Minor Mode
			[Description("None")]
			None = 0x00,

			[Description("Raw Histo")]
			RawHisto = 0x01,

			[Description("Raw Cap")]
			RawCap = 0x02,
		}

		public enum OperationMode_Minor_Manufactory : byte
		{
			// Minor Mode
			[Description("None")]
			None = 0x00,

			[Description("Raw Histogram")]
			RawHisto = 0x01,

			[Description("Raw Cap")]
			RawCap = 0x02,

			[Description("Point")]
			Point = 0x03,
		}

	}
}
