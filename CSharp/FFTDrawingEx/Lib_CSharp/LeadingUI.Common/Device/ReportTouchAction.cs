﻿using System;

namespace LeadingUI.Common.Device
{
	public enum ReportTouchAction
	{
		Unknown = 0,

		// Summary:
		//     The act of putting a finger onto the screen.
		Press = 1,
		//
		// Summary:
		//     The act of dragging a finger across the screen.
		Move = 2,
		//
		// Summary:
		//     The act of lifting a finger off of the screen.
		Release = 3,
	}
}
