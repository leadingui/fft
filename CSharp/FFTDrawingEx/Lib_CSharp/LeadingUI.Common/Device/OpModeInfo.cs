﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;

namespace LeadingUI.Common.Device
{
	// GET_OPMODE_INFO
	[StructLayout(LayoutKind.Sequential, Pack = 1)]
	public class OpModeInfo
	{
		public ushort I2cAddress;
		public byte ByteCount;
		public byte ChannelCount;
	}



}
