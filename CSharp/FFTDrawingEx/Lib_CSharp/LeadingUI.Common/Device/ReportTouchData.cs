﻿using System;
using System.Runtime.InteropServices;

namespace LeadingUI.Common.Device
{
	//터치 데이터는 32Bit Size 로 하기와 같은 형식으로 정의 된다. (Little EndianType)

	// ---------------------------------------------------------------------
	// | Name  | Bit Size     | Description                                |
	// |-------|--------------|--------------------------------------------|
	// |  X    | 12 (b11~b00) | X 좌표                                     |
	// |  Y    | 12 (b23~b12) | Y 좌표                                     |
	// |  ID   |  5 (b28~b24) | 좌표 Index                                 |
	// | Action|  2 (b30~b29) | 터치 타입 (1: Press, 2 : Move, 3 : Release)|
	// |  Rsv  |  1 (b31~b31) | Reserved                                   |
	// ---------------------------------------------------------------------

	[StructLayout(LayoutKind.Sequential, Pack = 1)]
	public class ReportTouchData
	{
		private UInt32 _data;

		public ReportTouchData(UInt32 value = 0)
		{
			_data = value;
		}

		/// <summary>
		/// size of ReportTouchData Structure
		/// </summary>
		/// <value>The size.</value>
		public static int Size
		{
			get { return Marshal.SizeOf(typeof(ReportTouchData)); }
		}

		public UInt32 Data
		{
			get { return _data; }
			set { _data = value; }
		}

		// pulbic uint X: 12 
		public uint X
		{
			get { return (uint)_data & 0xFFF; }
			set { _data = (_data & ~0xFFFu) | (value & 0xFFF); }
		}

		//public uint Y : 12   
		public uint Y
		{
			get { return (_data >> 12) & 0xFFF; }
			set { _data = (_data & ~(0xFFFu << 13)) | (value & 0xFFF) << 12; }
		}

		// public uint id: 5;  
		public uint Id
		{
			get { return (_data >> 24) & 0x1F; }
			set { _data = (_data & ~(0x1Fu << 24)) | (value & 0x1F) << 24; }
		}

		// public uint Type: 2
		public uint Action
		{
			get { return (_data >> 29) & 0x03; }
			set { _data = (_data & ~(0x03u << 29)) | (value & 0x03) << 29; }
		}

		public ReportTouchAction ReportTouchAction
		{
			get { return (ReportTouchAction)((_data >> 29) & 0x03); }
			set 
			{ 
				_data = (_data & ~(0x03u << 29)) | (((UInt32)value) & 0x03u) << 29;
			}
		}

		// public uint Reserved: 1
		//public uint Reserved
		//{
		//	get { return (_data >> 31) & 0x01; }
		//	set { _data = (_data & ~(0x3u << 30)) | (value & 0x3) << 30; }
		//}

		public byte[] GetBytes
		{
			get { return BitConverter.GetBytes(_data); }
		}

		public static ReportTouchData FromBytes(byte[] value, int startIndex)
		{
			if ((value == null) || (value.Length < 4))
				throw new ArgumentException("Invalid Parameter.");

			if ((startIndex < 0) || (value.Length < (startIndex + 4)))
				throw new ArgumentException("Invalid Parameter.");

			return new ReportTouchData(BitConverter.ToUInt32(value, startIndex));
		}
	} 

/*
	public struct ReportTouchData
	{
		internal static readonly BitVector32.Section X_Section    = BitVector32.CreateSection(0x7FF);
		internal static readonly BitVector32.Section Y_Section    = BitVector32.CreateSection(0x7FF, X_Section);
		internal static readonly BitVector32.Section ID_Section   = BitVector32.CreateSection(0x1F, Y_Section);
		internal static readonly BitVector32.Section Type_Section = BitVector32.CreateSection(0x3, ID_Section);
		internal static readonly BitVector32.Section Rsv_Section  = BitVector32.CreateSection(0x7, Type_Section);

		internal BitVector32 data;

		// public uint X: 11;  
		public uint X
		{
			get { return (uint)data[X_Section]; }
			set { data[X_Section] = (int)value; }
		}

		// public uint Y: 11;  
		public uint Y
		{
			get { return (uint)data[Y_Section]; }
			set { data[Y_Section] = (int)value; }
		}
		// public uint id: 5;  
		public uint Id
		{
			get { return (uint)data[ID_Section]; }
			set { data[ID_Section] = (int)value; }
		}
		// public uint Type: 2;  
		public uint Type
		{
			get { return (uint)data[Type_Section]; }
			set { data[Type_Section] = (int)value; }
		}
		// public uint Reserved: 3;  
		public uint Reserved
		{
			get { return (uint)data[Rsv_Section]; }
			set { data[Rsv_Section] = (int)value; }
		} 
	}
 */



/*
	//[StructLayout(LayoutKind.Sequential, Size = 4)]
	public struct ReportTouchData
	{
		[BitfieldLength(11)]
		public uint X;
		[BitfieldLength(11)]
		public uint Y;
		[BitfieldLength(5)]
		public uint Id;
		[BitfieldLength(2)]
		public uint Type;
		[BitfieldLength(3)]
		public uint Reserved;

		public static ReportTouchData FromUInteger(UInt32 val)
		{
			byte[] a = BitConverter.GetBytes(val);
			unsafe
			{
				fixed (byte* p = a)
				{
					return (ReportTouchData)Marshal.PtrToStructure((IntPtr)p, typeof(ReportTouchData));
				}
			}
		}

		public static ReportTouchData FromBytes(byte[] arVal)
		{
			if ((arVal == null) || (arVal.Length != 4))
				throw new ArgumentException("Invalid Parameter.");

			unsafe
			{
				fixed (byte* p = arVal)
				{
					return (ReportTouchData)Marshal.PtrToStructure((IntPtr)p, typeof(ReportTouchData));
				}
			}
		}

		public uint GetValue
		{
			get
			{
				return PrimitiveConversion.ToUInteger(this);
			}
		}
		
	};
*/

}
