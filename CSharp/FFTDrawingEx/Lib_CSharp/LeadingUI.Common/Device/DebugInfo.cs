﻿using System;
using System.Runtime.InteropServices;

namespace LeadingUI.Common.Device
{
	// GET_DEBUG_INFO command retrun value
	[StructLayout(LayoutKind.Sequential, Pack = 1)]
	public class DebugInfo
	{
		public byte byteCount;
		public byte channelCount;
	}
}
