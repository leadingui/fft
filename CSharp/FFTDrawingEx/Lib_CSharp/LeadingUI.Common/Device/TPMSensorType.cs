﻿using System;
using System.ComponentModel;

namespace LeadingUI.Common.Device
{
	/// <summary>
	/// Touch Controller Solution Types
	/// </summary>

	public enum TpmSensorType : byte
	{
		[Description("G1")]
		G1		= 0x01,

		[Description("P1")]
		P1		= 0x02,

		[Description("GF")]
		GF		= 0x03,

		[Description("PF")]
		PF		= 0x04,

		[Description("GG(SITO)")]
		GG_SITO	= 0x05,

		[Description("PG(SITO)")]
		PG_SITO	= 0x06,

		[Description("GG(DITO)")]
		GG_DITO	= 0x07,

		[Description("PG(DITO)")]
		PG_DITO	= 0x08,

		[Description("GFF")]
		GFF		= 0x09,

		[Description("PFF")]
		PFF		= 0x0A,
	}

	public class TpmSensorTypes
	{
		public const byte G1		= 0x01;
		public const byte P1		= 0x02;
		public const byte GF		= 0x03;
		public const byte PF		= 0x04;
		public const byte GG_SITO	= 0x05;
		public const byte PG_SITO	= 0x06;
		public const byte GG_DITO	= 0x07;
		public const byte PG_DITO	= 0x08;
		public const byte GFF		= 0x09;
		public const byte PFF		= 0x0A;
	}
}
