﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;

namespace LeadingUI.Common.Device
{
	public enum DebugPortType : byte
	{
		SPI = 0,
		I2C,
		INTR,
		MaxPortTypes,
	}

	[StructLayout(LayoutKind.Sequential, Pack = 1)]
	public class DebugPortMonitor
	{
		public DebugPortType PortType = DebugPortType.I2C;
		public bool    MonitorOnOff = false;
		public ushort  Address = 0;
		public ushort  Length = 0;
		public byte[]  Buffer;

		public DebugPortMonitor(DebugPortType pt, bool OnOff, ushort address, byte[] buffer, ushort bufferLength)
		{
			PortType = pt;
			MonitorOnOff = OnOff;

			if (pt == DebugPortType.I2C)
			{
				Address = address;
				Length = bufferLength;
				Buffer = buffer;
			}
			else if (pt == DebugPortType.SPI)
			{
				//Address = address;
				Length = bufferLength;
				Buffer = buffer;
			}
		}
	}
}
