﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace LeadingUI.Common.Device
{
	public enum DebugDataType : byte
	{
		[Description("Cap Data")]
		Cap        = 0x00,

		[Description("Histo Data")]
		Histo      = 0x01,
		
		[Description("Coordinate Data")]
		Coordinate = 0x02,
	}
}
