﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace LeadingUI.Common.Device
{
	public enum E2PRomSizeType : byte
	{
		[Description("8K")]
		Rom8K = 0x00,

		[Description("16K")]
		Rom16K = 0x01,
		
		[Description("30K")]
		Rom30K = 0x02,
		
		[Description("32K")]
		Rom32K = 0x03,

		[Description("62K")]
		Rom62K = 0x04,

		[Description("64K")]
		Rom64K = 0x05,
	}

	public class E2PRomSizeTypes
	{
		public const byte Rom8K  = 0x00;
		public const byte Rom16K = 0x01;
		public const byte Rom30K = 0x02;
		public const byte Rom32K = 0x03;
		public const byte Rom62K = 0x04;
		public const byte Rom64K = 0x05;
	}

	public class E2PRomSizes
	{
		public const UInt16 Rom8K  = 0x2000;
		public const UInt16 Rom16K = 0x4000;
		public const UInt16 Rom30K = 0x7800;
		public const UInt16 Rom32K = 0x8000;
		public const UInt32 Rom62K = 0xF800;
		public const UInt32 Rom64K = 0x10000;
	}



}
