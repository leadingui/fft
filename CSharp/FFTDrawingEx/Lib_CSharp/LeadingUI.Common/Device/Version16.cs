﻿using System;
using System.Runtime.InteropServices;

namespace LeadingUI.Common.Device
{
	[StructLayout(LayoutKind.Sequential, Pack = 1)]
	[Serializable()]
	public class Version16
	{
		//private byte major = 0x00;
		//private byte minor = 0x00;
		private ushort _version = 0x0000;

		public Version16()
		{
			_version = 0;
		}

		public Version16(byte major, byte minor)
		{
			//this.Major = major;
			//this.Minor = minor;
			_version = (ushort)((major << 8) | (minor));
		}

		public Version16(Version16 pre)
		{
			if (pre != null)
			{
				//Major = version.Major;
				//Minor = version.Minor;
				_version = pre.UInt16;
			}
		}

		public Version16(UInt16 version)
		{
			//major = (byte)(version >> 8);
			//minor = (byte)(version & 0x00FF);
			_version = version;
		}

		public ushort UInt16
		{
			//get { return (ushort)((major << 8) | (minor)); }
			//set
			//{
			//	major = (byte)(value >> 8);
			//	minor = (byte)(value & 0x00FF);
			//}
			get { return _version; }
			set { _version = value; }
		}

		public byte Major
		{
			//get { return major; }
			//set { major = value; }
			get { return (byte)(_version >> 8); }
			set
			{
				_version = (ushort)((value << 8) | (Minor));
			}
		}

		public byte Minor
		{
			//get { return minor; }
			//set { minor = value; }
			get { return (byte)(_version & 0x00FF); }
			set
			{
				_version = (ushort)((Major << 8) | (value));
			}
		}

		public override string ToString()
		{
			return string.Format("{0}.{1}", Major, Minor);
		}

		public int Compare(Version16 pre)
		{
			if (_version > pre.UInt16)
				return 1;
			else if (_version == pre.UInt16)
				return 0;
			else
				return -1;
		}

		public bool CompareTo(Version16 pre)
		{
			return (_version == pre.UInt16);
		}

		public byte[] GetBytes()
		{
			return BitConverter.GetBytes(_version);
		}

		public static int Size
		{
			get { return Marshal.SizeOf(typeof(Version8)); }
		}

	}

}
