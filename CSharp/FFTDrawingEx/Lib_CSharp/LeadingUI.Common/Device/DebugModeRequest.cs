﻿using LeadingUI.Common.UsbBridge;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;

namespace LeadingUI.Common.Device
{
	//[StructLayout(LayoutKind.Sequential, Pack = 1)]
	public class DebugModeRequest
	{
		private OperationMode opMode;
		private object        _data;

		public DebugModeRequest(OperationMode mode, object data)
		{
			opMode = mode;
			_data = data;
		}

		public DebugModeRequest(BridgeCommand cmd, OperationMode mode)
		{
			opMode = mode;

			switch (cmd)
			{
			//case BridgeCommand.GetOpModeInfo:
			//    _data = new OpModeInfo();
			//    break;
			//case BridgeCommand.GetDebugInfo:
			//    _data = new DebugInfo();
			//    break;
			default:
				throw new Exception("Invalid Command");
				//break;
			}
		}
		
		public OperationMode OperationMode
		{
			get
			{
				return opMode;
			}
		}
	
		public object Data
		{
			get
			{
				return _data;
			}

			set
			{
				_data = value;
			}

		}

		public OpModeInfo OpModeInfo
		{
			get
			{
				return (OpModeInfo)_data;
			}
			set
			{
				_data = value;
			}
		}

		public DebugInfo DebugInfo
		{
			get
			{
				return (DebugInfo)_data;
			}
			set
			{
				_data = value;
			}
		}
	}
}
