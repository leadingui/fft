﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;

namespace LeadingUI.Common.Device
{
	public class DeviceIDS
	{
		public const ushort LU2010 = 0x2010;
		public const ushort LU2020 = 0x2020;
		public const ushort LU3010 = 0x3010;
		public const ushort LU3100 = 0x3100;
		public const ushort LU310A = 0x310A;
		public const ushort LU3200 = 0x3200;
	}

	// Chip ID
	[StructLayout(LayoutKind.Sequential, Pack = 1)]
	public class DeviceID
	{
		public ushort ID = 0x0000;

		public DeviceID( ushort id = 0x0000)
		{
			ID = id;
		}

		public bool IsValidID
		{
			get
			{
				if (ID == DeviceID.LU2010)
					return true;

				if (ID == DeviceID.LU2020)
					return true;

				if (ID == DeviceID.LU3010)
					return true;

				if (ID == DeviceID.LU3100)
					return true;

				if (ID == DeviceID.LU310A)
					return true;

				if (ID == DeviceID.LU3200)
					return true;

				return false;
			}
		}
		public bool IsLU2010
		{
			get
			{
				if (ID == DeviceID.LU2010)
					return true;
				else
					return false;
			}
		}
		public bool IsLU2020
		{
			get
			{
				if (ID == DeviceID.LU2020)
					return true;
				else
					return false;
			}
		}
		public bool IsLU3010
		{
			get
			{
				if (ID == DeviceID.LU3010)
					return true;
				else
					return false;
			}
		}

		public bool IsLU3100
		{
			get
			{
				if (ID == DeviceID.LU3100)
					return true;
				else
					return false;
			}
		}

		public bool IsLU310A
		{
			get
			{
				if (ID == DeviceID.LU310A)
					return true;
				else
					return false;
			}
		}

		public bool IsLU3200
		{
			get
			{
				if (ID == DeviceID.LU3200)
					return true;
				else
					return false;
			}
		}

		public const ushort LU2010 = 0x2010;
		public const ushort LU2020 = 0x2020;
		public const ushort LU3010 = 0x3010;
		public const ushort LU3100 = 0x3100;
		public const ushort LU310A = 0x310A;
		public const ushort LU3200 = 0x3200;
	}
}
