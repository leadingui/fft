﻿using System;
using System.Runtime.InteropServices;

namespace LeadingUI.Common.Device
{
	[StructLayout(LayoutKind.Sequential, Pack = 1)]
	public struct ReportPressureData
	{
		private UInt16 _data;

		public ReportPressureData(UInt16 data = 0)
		{
			_data = data;
		}

		public ReportPressureData(byte histoValue, byte histoCount)
		{
			_data = (UInt16)((UInt16)histoValue | (UInt16)(histoCount << 8));
		}

		/// <summary>
		/// size of ReportPressureData Structure
		/// </summary>
		/// <value>The length.</value>
		public static int Size
		{
			get { return Marshal.SizeOf(typeof(ReportPressureData)); }
		}

		public UInt16 Data
		{
			get { return _data; }
			set { _data = value; }
		}

		public byte HistoValue
		{
			get 
			{ 
				return (byte)(_data); 
			}
			set 
			{ 
				_data = (UInt16)((_data & 0xFF00) | (UInt16)value); 
			}
		}

		public byte HistoCount
		{
			get 
			{ 
				return (byte)(_data >> 8); 
			}

			set
			{
				_data = (UInt16)((_data & 0x00FF) | ((UInt16)value << 8));
			}
		}

		public byte[] GetBytes
		{
			get { return BitConverter.GetBytes(_data); }
		}
	}
}
