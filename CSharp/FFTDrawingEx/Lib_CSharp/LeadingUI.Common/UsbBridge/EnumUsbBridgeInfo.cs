﻿//using LeadingUI.Port.Usb;
using LeadingUI.Common.Usb;
using System;
using System.Collections.Generic;
//using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace LeadingUI.Common.UsbBridge
{
	[StructLayout(LayoutKind.Sequential, Pack = 1)]
	public class EnumUsbBridgeInfo
	{
		/// Bridge Board가 저장되어 있는 Symbol 경로
		[MarshalAs(UnmanagedType.ByValArray, SizeConst = 260)]
		private byte[] _path = new byte[260];

		/// Bridge Board가 가지고 있는 Vender ID
		private USB_ID _usb_id;

		/// Bridge Board의 정보
		private UsbBridgeInfo _usbBridgeInfo;

		/// Bridge Board에 연결되 Device ID
		//public ushort device_id;

		public EnumUsbBridgeInfo()
		{
			this._path          = new byte[260];
			this._usbBridgeInfo = new UsbBridgeInfo();
			//this.device_id = 0;
		}

		public EnumUsbBridgeInfo(EnumUsbBridgeInfo info)
		{
			this._path          = new byte[260];
			this._usbBridgeInfo = new UsbBridgeInfo();

			Buffer.BlockCopy(info.Path, 0, this._path, 0, 260);

			this._usb_id.Vendor = info.UsbId.Vendor;
			this._usb_id.Product = info.UsbId.Product;

			this._usbBridgeInfo = new UsbBridgeInfo(info.UsbBridgeInfo);

			//this.device_id = info.device_id;
		}


		public byte[] Path
		{
			get { return this._path; }
		}

		public USB_ID UsbId
		{
			get { return _usb_id; }
			set
			{
				this._usb_id.Vendor = value.Vendor;
				this._usb_id.Product = value.Product;
			}
		}

		public UsbBridgeInfo UsbBridgeInfo
		{
			get { return this._usbBridgeInfo; }
			set
			{
				this._usbBridgeInfo = new UsbBridgeInfo(value);
			}
		}


		public static int Size
		{
			 get
			 {
				 return Marshal.SizeOf(typeof(EnumUsbBridgeInfo));
			 }
		}

	}
}
