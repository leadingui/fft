﻿using LeadingUI.Common.Device;
using System;
using System.Runtime.InteropServices;
using System.Text;


namespace LeadingUI.Common.UsbBridge
{

	/// <summary>
	/// USB Bridge Board의 정보
	/// </summary>
	[StructLayout(LayoutKind.Sequential, Pack = 1)]
	public class UsbBridgeInfo
	{
		[MarshalAs(UnmanagedType.ByValArray, SizeConst = 32)]
		private byte[]    _name;
		private Version16 _version;
		private byte      _id;

		public UsbBridgeInfo()
		{
			_name = new Byte[32];
			_version = new Version16();
			_id = 0;
		}

		public UsbBridgeInfo(UsbBridgeInfo info)
		{
			Name = info.Name;
			Version = info.Version;
			Id = info.Id;
		}

		public string Name 
		{
			get 
			{ 
				int length = Array.IndexOf(_name, (byte)0);
				if (length < 0)
					length = _name.Length;
				return Encoding.ASCII.GetString(_name, 0, length);
			}

			set
			{
				if (_name == null)
					_name = new byte[32];

				byte[] arModel = ASCIIEncoding.ASCII.GetBytes(value);
				Buffer.BlockCopy(arModel, 0, _name, 0, (arModel.Length > 32) ? 32 : arModel.Length);
			}
		}
		public Version16 Version 
		{ 
			get 
			{ 
				return _version; 
			}

			set
			{
				_version = new Version16(value);
			}
		}

		public int Id 
		{ 
			get 
			{ 
				return _id; 
			}
			set
			{
				_id = (byte)value;
			}
		}
	}

}
