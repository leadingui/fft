﻿using System;

namespace LeadingUI.Common.UsbBridge
{
	public enum BridgeCommand : byte
	{
		BridgeInfo      = 0xA0,
		ErrorStatus     = 0xA1,
		SetDeviceGroup  = 0xA2,
		SetOpMode       = 0xA3,
		DeviceReset     = 0xA4,
		EEPromReadWrite = 0xA5,
		EEPromAccess    = 0xA6,
		EEPromEraseAll  = 0xA7,
		I2cPortMonitor  = 0xA8,
		SpiPortMonitor  = 0xA9,
		DeviceInfo      = 0xAA,
		FindI2cAddress  = 0xAB,
		BridgeReset     = 0xAC,
		IntPortMonitor  = 0xAD,
		GetDeviceID     = 0xAE,
		DeviceInfoHeader= 0xAF,

		SetTspPowerOnOff  = 0xB0,
		GetTspPowerStatus = 0xB1,
   }

	public class BridgeCommands
	{
		public const byte BridgeInfo      = (byte)BridgeCommand.BridgeInfo;		//0xA0;
		public const byte ErrorStatus     = (byte)BridgeCommand.ErrorStatus;	//0xA1;
		public const byte SetDeviceGroup  = (byte)BridgeCommand.SetDeviceGroup;	//0xA2;
		public const byte SetOpMode       = (byte)BridgeCommand.SetOpMode;		//0xA3;
		public const byte DeviceReset     = (byte)BridgeCommand.DeviceReset;	//0xA4;
		public const byte EEPromReadWrite = (byte)BridgeCommand.EEPromReadWrite;//0xA5;
		public const byte EEPromAccess    = (byte)BridgeCommand.EEPromAccess;	//0xA6;
		public const byte EEPromEraseAll  = (byte)BridgeCommand.EEPromEraseAll;	//0xA7;
		public const byte I2cPortMonitor  = (byte)BridgeCommand.I2cPortMonitor;	//0xA8;
		public const byte SpiPortMonitor  = (byte)BridgeCommand.SpiPortMonitor;	//0xA9;
		public const byte DeviceInfo      = (byte)BridgeCommand.DeviceInfo;		//0xAA;
		public const byte FindI2cAddress  = (byte)BridgeCommand.FindI2cAddress;	//0xAB;
		public const byte BridgeReset     = (byte)BridgeCommand.BridgeReset;	//0xAC;
		public const byte IntPortMonitor  = (byte)BridgeCommand.IntPortMonitor;	//0xAD;
		public const byte GetDeviceID	  = (byte)BridgeCommand.GetDeviceID;	//0xAE;
		public const byte DeviceInfoHeader = (byte)BridgeCommand.DeviceInfoHeader;//0xAF;

		public const byte SetTspPowerOnOff  = (byte)BridgeCommand.SetTspPowerOnOff;  // 0xB0,
		public const byte GetTspPowerStatus = (byte)BridgeCommand.GetTspPowerStatus; // 0xB1,
	}




}
