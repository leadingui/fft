﻿using System;

namespace LeadingUI.Common.UsbBridge
{
	public class BridgeEndpointTypes
	{
		public const byte SPI  = 0x01;
		public const byte I2C  = 0x02;
		public const byte INTR = 0x03;
	}

	public class BridgeEndpointAddress
	{
		public const byte SPI  = 0x81;
		public const byte I2C  = 0x82;
		public const byte INTR = 0x83;
	}

}
