﻿using LeadingUI.Common.Device;
using LeadingUI.Common.Usb;
using System;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.InteropServices;

namespace LeadingUI.Common.UsbBridge
{
	[StructLayout(LayoutKind.Sequential, Pack = 1)]
	public struct RequestData
	{
		public int Length;    // data buffer length
		public byte[] Buffer;    // buffer

		public RequestData(int length)
		{
			this.Length = length;

			if (length > 0)
			{
				this.Buffer = new byte[length];
			}
			else
			{
				// Buffer를 null로 설정하면 WinUsb_xxx함수 호출시 문제가 발생한다.
				// 그러므로 최소 크기로 설정한다.
				//this.Buffer = null;
				this.Buffer = new byte[1];
			}
		}
	}

	//[StructLayout(LayoutKind.Sequential, Pack = 1)]
	//[StructLayout(LayoutKind.Explicit, Size = 16, CharSet = CharSet.Ansi)]
	//[StructLayout(LayoutKind.Explicit, CharSet = CharSet.Ansi)]
	[StructLayout(LayoutKind.Explicit)]
	public class BridgeRequestBlock// : IDisposable
	{
		[FieldOffset(0)] public byte   Command;
		[FieldOffset(1)] public ushort TimeOut;
		[FieldOffset(3)] public ushort Parameter0;
		[FieldOffset(5)] public ushort Parameter1;
		[FieldOffset(7)] public ushort Parameter2;
		[FieldOffset(9)] public ushort Parameter3;
		[FieldOffset(11)] private IntPtr RequestDataPtr;    // buffer
		
		#region SetDeviceGroup
		[FieldOffset(3)] public byte Device;  // parameter0
		[FieldOffset(4)] public byte Group;	  // parameter0
		#endregion

		#region SetOpMode
		[FieldOffset(3)] public byte OperationMode_Minor;
		[FieldOffset(4)] public byte OperationMode_Major;
		#endregion

		#region EEPromReadWrite / ReadEEPROM
		[FieldOffset(3)] public int EEPromRW;
		[FieldOffset(7)] public int EEPromAddress;
		#endregion

		#region EEPromAccess
		[FieldOffset(3)] public byte EEPromAccessMode;
		#endregion

		#region I2cPortMonitor
		[FieldOffset(3)] public int I2cMonAddress;
		[FieldOffset(7)] public int I2cMonLength;
		#endregion

		#region SpiPortMonitor
		[FieldOffset(3)] public ushort SpiMonStatus;
		#endregion

		#region ReloadData
		[FieldOffset(3)] public ushort ReloadValue;
		#endregion

		#region GetOpModeInfo
		[FieldOffset(3)] public ushort DebugMode;
		#endregion

		public BridgeRequestBlock()
		{
			Command			= 0x00;
			TimeOut			= 3;
			Parameter0		= 0;
			Parameter1		= 0;
			Parameter2		= 0;
			Parameter3		= 0;
			RequestDataPtr	= IntPtr.Zero;
		}

		~ BridgeRequestBlock()
		{
			if (RequestDataPtr != IntPtr.Zero)
				Marshal.Release(RequestDataPtr);
			//Dispose(false);
		}


		public RequestData RequestData
		{
			get
			{
				return (RequestData)Marshal.GetObjectForIUnknown(RequestDataPtr);							
			}

			set
			{
				RequestDataPtr = Marshal.GetIUnknownForObject(value);
			}
		}

		public int RequestDataLength
		{
			get
			{
				return this.RequestData.Length;
			}
		}

		#region Make SetupPacket

		public static SetupPacket GetSetupPacket(BridgeRequestBlock brb)
		{
			SetupPacket setupPacket = new SetupPacket();

			RequestData requestData = brb.RequestData;

			switch (brb.Command)
			{
			case BridgeCommands.BridgeInfo:
				{
					setupPacket.bmRequestType = RequestTypes.VendorRead;
					setupPacket.bRequest      = brb.Command;
					setupPacket.wValue        = 0;
					setupPacket.wIndex        = brb.TimeOut;
					setupPacket.wLength       = (ushort)brb.RequestData.Length;
				}
				break;
			case BridgeCommands.ErrorStatus:
				{
					setupPacket.bmRequestType = RequestTypes.VendorRead;
					setupPacket.bRequest      = brb.Command;
					setupPacket.wValue        = 0;
					setupPacket.wIndex        = brb.TimeOut;
					setupPacket.wLength       = (ushort)brb.RequestData.Length;
				}
				break;
			case BridgeCommands.SetDeviceGroup:
				{
					setupPacket.bmRequestType = RequestTypes.VendorWrite;
					setupPacket.bRequest      = brb.Command;
					setupPacket.wValue        = brb.Parameter0;
					setupPacket.wIndex        = brb.TimeOut;
					setupPacket.wLength       = (ushort)brb.RequestData.Length;
				}
				break;
			case BridgeCommands.SetOpMode:
				 {
					setupPacket.bmRequestType = RequestTypes.VendorWrite;
					setupPacket.bRequest      = brb.Command;
					setupPacket.wValue        = brb.Parameter0;
					setupPacket.wIndex        = brb.TimeOut;
					setupPacket.wLength       = (ushort)brb.RequestData.Length;
				}
				break;
			case BridgeCommands.DeviceReset:
				 {
					setupPacket.bmRequestType = RequestTypes.VendorWrite;
					setupPacket.bRequest      = brb.Command;
					setupPacket.wValue        = 0;
					setupPacket.wIndex        = brb.TimeOut;
					setupPacket.wLength       = (ushort)brb.RequestData.Length;
				}
				break;
			case BridgeCommands.EEPromReadWrite:
				  {
					  if (brb.EEPromRW == 0)
						setupPacket.bmRequestType = RequestTypes.VendorWrite;
					else
						setupPacket.bmRequestType = RequestTypes.VendorRead;

					setupPacket.bRequest      = brb.Command;
					setupPacket.wValue        = (ushort)brb.EEPromAddress;
					setupPacket.wIndex        = brb.TimeOut;
					setupPacket.wLength       = (ushort)brb.RequestData.Length;
				}
				break;
			//case BridgeCommands.EEPromReadWrite2:
			//	  {
			//		  if (brb.EEPromRW == 0)
			//			setupPacket.bmRequestType = RequestTypes.VendorWrite;
			//		else
			//			setupPacket.bmRequestType = RequestTypes.VendorRead;

			//		setupPacket.bRequest      = brb.Command;
			//		setupPacket.wValue        = (ushort)brb.EEPromAddress;
			//		setupPacket.wIndex        = (ushort)(((UInt32)brb.EEPromAddress) >> 16);
			//		setupPacket.wLength       = (ushort)brb.RequestData.Length;
			//	}
			//	break;
			case BridgeCommands.EEPromAccess:
				  {
					setupPacket.bmRequestType = RequestTypes.VendorWrite;
					setupPacket.bRequest      = brb.Command;
					setupPacket.wValue        = brb.EEPromAccessMode;
					setupPacket.wIndex        = brb.TimeOut;
					setupPacket.wLength       = (ushort)brb.RequestData.Length;
				}
				break;
			case BridgeCommands.EEPromEraseAll:
				{
					setupPacket.bmRequestType = RequestTypes.VendorWrite;
					setupPacket.bRequest      = brb.Command;
					setupPacket.wValue        = 0;
					setupPacket.wIndex        = brb.TimeOut;
					setupPacket.wLength       = (ushort)brb.RequestData.Length;
				}
				break;
			case BridgeCommands.I2cPortMonitor:
				{
					setupPacket.bmRequestType = RequestTypes.VendorWrite;
					setupPacket.bRequest      = brb.Command;
					setupPacket.wValue        = (ushort)brb.I2cMonAddress;
					setupPacket.wIndex        = (ushort)brb.I2cMonLength;   // zero is monitor off, other is start
					setupPacket.wLength       = (ushort)brb.RequestData.Length;
				}
				break;
			case BridgeCommands.SpiPortMonitor:
				{
					setupPacket.bmRequestType = RequestTypes.VendorWrite;
					setupPacket.bRequest      = brb.Command;
					setupPacket.wValue        = brb.SpiMonStatus; // 0: off, 1: On
					setupPacket.wIndex        = 0;
					setupPacket.wLength       = (ushort)brb.RequestData.Length;
				}
				break;
			case BridgeCommands.IntPortMonitor:
				{
					throw new Exception("Failed to invalid command(can't supported).");
				}
				//break;
			case BridgeCommands.DeviceInfo:
				{
					setupPacket.bmRequestType = RequestTypes.VendorRead;
					setupPacket.bRequest      = brb.Command;
					setupPacket.wValue        = 0;
					setupPacket.wIndex        = brb.TimeOut;
					setupPacket.wLength       = (ushort)brb.RequestData.Length;
				}
				break;
			case BridgeCommands.FindI2cAddress:
				{
					setupPacket.bmRequestType = RequestTypes.VendorWrite;
					setupPacket.bRequest      = brb.Command;
					setupPacket.wValue        = 0;
					setupPacket.wIndex        = brb.TimeOut;
					setupPacket.wLength       = (ushort)brb.RequestData.Length;
				}
				break;
			case BridgeCommands.BridgeReset:
				{
					setupPacket.bmRequestType = RequestTypes.VendorWrite;
					setupPacket.bRequest      = brb.Command;
					setupPacket.wValue        = 0;
					setupPacket.wIndex        = 0;
					setupPacket.wLength       = (ushort)brb.RequestData.Length;
				}
				break;
			case BridgeCommands.GetDeviceID:
				{
					setupPacket.bmRequestType = RequestTypes.VendorRead;
					setupPacket.bRequest      = brb.Command;
					setupPacket.wValue        = 0;
					setupPacket.wIndex        = brb.TimeOut;
					setupPacket.wLength       = (ushort)brb.RequestData.Length;
				}
				break;
			case BridgeCommands.SetTspPowerOnOff:
				{
					setupPacket.bmRequestType = RequestTypes.VendorWrite;
					setupPacket.bRequest      = brb.Command;
					setupPacket.wValue        = 0;
					setupPacket.wIndex        = brb.TimeOut;
					setupPacket.wLength       = (ushort)brb.RequestData.Length;
				}
				break;
			case BridgeCommands.GetTspPowerStatus:
				{
					setupPacket.bmRequestType = RequestTypes.VendorRead;
					setupPacket.bRequest      = brb.Command;
					setupPacket.wValue        = 0;
					setupPacket.wIndex        = brb.TimeOut;
					setupPacket.wLength       = (ushort)brb.RequestData.Length;
				}
				break;

		   default:
				throw new Exception("Failed to invalid command.");
				//break;
			}

			return setupPacket;
		}

		#endregion

		#region Make Bridge Request Block

		public static BridgeRequestBlock BridgeInfo(ushort bufferLength, ushort timeOut = 3)
		{
			BridgeRequestBlock brb = new BridgeRequestBlock();
			brb.Command = BridgeCommands.BridgeInfo;
			brb.TimeOut = timeOut;
			brb.RequestData = new RequestData(bufferLength);
			return brb;
		}

		public static BridgeRequestBlock ErrorStatus(ushort bufferLength, ushort timeOut = 3)
		{
			BridgeRequestBlock brb = new BridgeRequestBlock();
			brb.Command = BridgeCommands.ErrorStatus;
			brb.TimeOut = timeOut;
			brb.RequestData = new RequestData(bufferLength);
			return brb;
		}

		public static BridgeRequestBlock SetDeviceGroup(ProductGroup group, DeviceType id, ushort timeOut = 3)
		{
			BridgeRequestBlock brb = new BridgeRequestBlock();
			brb.Command    = BridgeCommands.SetDeviceGroup;
			//brb.Parameter0 = DeviceGroup.GetDeviceGroup(group, id);
			brb.Group = (byte)group;
			brb.Device = (byte)id;
			brb.TimeOut = timeOut;
			brb.RequestData = new RequestData(0);
			return brb;
		}

		public static BridgeRequestBlock SetDeviceGroup(ushort deviceGroupId, ushort timeOut = 3)
		{
			BridgeRequestBlock brb = new BridgeRequestBlock();
			brb.Command = BridgeCommands.SetDeviceGroup;
			brb.Parameter0 = deviceGroupId;
			brb.TimeOut = timeOut;
			brb.RequestData = new RequestData(0);
			return brb;
		}

		public static BridgeRequestBlock SetDeviceGroup(DeviceGroup group, ushort timeOut = 3)
		{
			BridgeRequestBlock brb = new BridgeRequestBlock();
			brb.Command = BridgeCommands.SetDeviceGroup;
			brb.Group = (byte)group.ProductGroup;
			brb.Device = (byte)group.DeviceType;
			//brb.Parameter0 = group.Id;
			brb.TimeOut = timeOut;
			brb.RequestData = new RequestData(0);
			return brb;
		}

		public static BridgeRequestBlock SetOpMode(byte major, byte minor, ushort timeOut = 3)
		{
			BridgeRequestBlock brb = new BridgeRequestBlock();
			brb.Command = BridgeCommands.SetOpMode;
			brb.OperationMode_Major = major;
			brb.OperationMode_Minor = minor;
			brb.TimeOut = timeOut;
			brb.RequestData = new RequestData(0);
			return brb;
		}

		public static BridgeRequestBlock SetOpMode(OperationMode opMode, ushort timeOut = 3)
		{
			BridgeRequestBlock brb = new BridgeRequestBlock();
			brb.Command = BridgeCommands.SetOpMode;
			brb.OperationMode_Major = opMode.Major;
			brb.OperationMode_Minor = opMode.Minor;
			brb.TimeOut = timeOut;
			brb.RequestData = new RequestData(0);
			return brb;
		}

		public static BridgeRequestBlock DeviceReset(ushort timeOut = 3)
		{
			BridgeRequestBlock brb = new BridgeRequestBlock();
			brb.Command            = BridgeCommands.DeviceReset;
			brb.TimeOut            = timeOut;
			brb.RequestData        = new RequestData(0);
			return brb;
		}

		public static BridgeRequestBlock EEPromReadWrite(bool read, int address, int bufferLength, ushort timeOut = 3)
		{
			BridgeRequestBlock brb = new BridgeRequestBlock();
			brb.Command            = BridgeCommands.EEPromReadWrite;
			brb.EEPromRW           = (read) ? (ushort)1 : (ushort)0;
			brb.EEPromAddress      = address;
			brb.TimeOut            = timeOut;
			brb.RequestData        = new RequestData(bufferLength);
			return brb;
		}

		//public static BridgeRequestBlock EEPromReadWrite2(bool read, int address, int bufferLength)
		//{
		//	BridgeRequestBlock brb = new BridgeRequestBlock();
		//	brb.Command            = BridgeCommands.EEPromReadWrite2;
		//	brb.EEPromRW           = (read) ? (ushort)1 : (ushort)0;
		//	brb.EEPromAddress      = address;			
		//	brb.RequestData        = new RequestData(bufferLength);
		//	return brb;
		//}

		public static BridgeRequestBlock EEPromAccess(bool on, ushort timeOut = 3)
		{
			BridgeRequestBlock brb = new BridgeRequestBlock();
			brb.Command = BridgeCommands.EEPromAccess;
			brb.EEPromAccessMode = on ? (byte)1 : (byte)0;
			brb.TimeOut = timeOut;
			brb.RequestData = new RequestData(0);
			return brb;
		}

		public static BridgeRequestBlock EEPromEraseAll(ushort timeOut = 3)
		{
			BridgeRequestBlock brb = new BridgeRequestBlock();
			brb.Command = BridgeCommands.EEPromEraseAll;
			brb.TimeOut = timeOut;
			brb.RequestData = new RequestData(0);
			return brb;
		}

		public static BridgeRequestBlock I2cPortMonitor(bool on, ushort address, ushort DataLength, ushort timeOut = 3)
		{
			BridgeRequestBlock brb = new BridgeRequestBlock();

			brb.Command       = BridgeCommands.I2cPortMonitor;
			brb.I2cMonAddress = address;

			if (on)
			{
				brb.I2cMonLength = DataLength;   // zero is monitor off, other is start
				brb.RequestData  = new RequestData(0); // 읽어갈 데이터는 EP1or EP2이기 때문에 여기서는 buffer를 설정할 필요없다.
			}
			else
			{
				brb.I2cMonLength = 0; // zero is monitor off, other is start
				brb.RequestData   = new RequestData(0);
			}
			return brb;
		}

		public static BridgeRequestBlock SpiPortMonitor(bool on, ushort timeOut = 3)
		{
			BridgeRequestBlock brb = new BridgeRequestBlock();
			brb.Command			   = BridgeCommands.SpiPortMonitor;
			brb.SpiMonStatus	   = on ? (ushort)1 : (ushort)0;
			brb.RequestData		   = new RequestData(0);
			return brb;
		}

		public static BridgeRequestBlock DeviceInfo(ushort bufferLength, ushort timeOut = 3)
		{
			BridgeRequestBlock brb = new BridgeRequestBlock();
			brb.Command = BridgeCommands.DeviceInfo;
			brb.TimeOut = timeOut;
			brb.RequestData = new RequestData(bufferLength);
			return brb;
		}

		public static BridgeRequestBlock FindI2cAddress(ushort timeOut = 3)
		{
			BridgeRequestBlock brb = new BridgeRequestBlock();
			brb.Command = BridgeCommands.FindI2cAddress;
			brb.TimeOut = timeOut;
			brb.RequestData = new RequestData(0);
			return brb;
		}

		public static BridgeRequestBlock BridgeReset(ushort timeOut = 3)
		{
			BridgeRequestBlock brb = new BridgeRequestBlock();
			brb.Command = BridgeCommands.BridgeReset;
			//brb.TimeOut = timeOut;
			brb.RequestData = new RequestData(0);
			return brb;
		}


		public static BridgeRequestBlock GetDeviceID(ushort bufferLength, ushort timeOut = 3)
		{
			BridgeRequestBlock brb = new BridgeRequestBlock();
			brb.Command = BridgeCommands.GetDeviceID;
			brb.TimeOut = timeOut;
			brb.RequestData = new RequestData(bufferLength);
			return brb;
		}


		public static BridgeRequestBlock SetTspPowerOnOff(ushort bufferLength, ushort timeOut = 3)
		{
			BridgeRequestBlock brb = new BridgeRequestBlock();
			brb.Command = BridgeCommands.SetTspPowerOnOff;
			brb.TimeOut = timeOut;
			brb.RequestData = new RequestData(bufferLength);
			return brb;
		}

		public static BridgeRequestBlock GetTspPowerStatus(ushort bufferLength, ushort timeOut = 3)
		{
			BridgeRequestBlock brb = new BridgeRequestBlock();
			brb.Command = BridgeCommands.GetTspPowerStatus;
			brb.TimeOut = timeOut;
			brb.RequestData = new RequestData(bufferLength);
			return brb;
		}
		
		#endregion
	}
}
