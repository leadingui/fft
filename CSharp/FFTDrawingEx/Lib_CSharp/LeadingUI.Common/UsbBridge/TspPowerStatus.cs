﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;

namespace LeadingUI.Common.UsbBridge
{
	// -----------------------------------------------------
	// |         |  Write Mode      |      Read Mode       |
	// -----------------------------------------------------
	// | Power   |  0: Power Off, 1: Power On              |
	// -----------------------------------------------------
	// | VCC     |  공급전원: 10 mV단위, 유효범위 18~33    |
	// -----------------------------------------------------
	// | VIO	 |  IO 전원: 100 mV단위, 유효범위 18~33    |
	// -----------------------------------------------------
	// | Current |  Current Limit   |  계측된 Current 값   |
	// |         |  1 ~ 255         |                      |
	// -----------------------------------------------------

	[StructLayout(LayoutKind.Sequential, Pack = 1)]
	public class TspPowerStatus
	{
		byte _power;
		byte _vcc;
		byte _vio;
		byte _current;

        public TspPowerStatus()
        {
            _power   = 0;
            _vcc     = 33;
            _vio     = 33;
            _current = 50;
        }

		public TspPowerStatus(byte vcc, byte vio, byte current)
		{
			_power = 0;
			_vcc = vcc;
			_vio = vio;
			_current = current;
		}

		public byte Current
		{
			get { return this._current; }
			set
			{
				if (value <= 0)
				{
					throw new Exception("Invalid range");
				}

				if (value <= 0)
					this._current = 1;
				else
					this._current = value;
			}
		}

		public bool PowerOnOff
		{
			get
			{
				if (this._power > 0)
					return true;
				else
					return false;
			}
			set
			{
				if (value)
					_power = 1;
				else
					_power = 0;
			}
		}

		public byte VCC
		{
			get { return this._vcc; }
			set
			{
				if ((value < 18) && (value > 33))
				{
					throw new Exception("Invalid range");
				}

				if (value < 18)
					this._vcc = 18;
				else if (value > 33)
					this._vcc = 33;
				else
					this._vcc = value;
			}
		}

		public byte VIO
		{
			get { return this._vio; }
			set
			{
				if ((value < 18) && (value > 33))
				{
					throw new Exception("Invalid range");
				}

				if (value < 18)
					this._vio = 18;
				else if (value > 33)
					this._vio = 33;
				else
					this._vio = value;
			}
		}

		public static int Size
		{
			get { return Marshal.SizeOf(typeof(TspPowerStatus)); }
		}

		public byte[] GetBytes
		{
			get
			{
				int size = Marshal.SizeOf(typeof(TspPowerStatus));
				byte[] buff = new byte[size];
				buff[0] = _power;
				buff[1] = _vcc;
				buff[2] = _vio;
				buff[3] = _current;

				return buff;
			}
		}
	}
}
