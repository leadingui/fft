﻿using System;
using System.ComponentModel;
using System.Runtime.InteropServices;
using System.Security;
using System.Security.Permissions;

namespace LeadingUI.Common
{
    /// <summary>
    /// Exception used internally to catch Win32 API errors. This exception should
    /// not be thrown to the library's caller.
    /// </summary>
	[Serializable]
	[SuppressUnmanagedCodeSecurity]
	public class ApiException : Exception
	{
		public ApiException(string message) :
			base(message)
		{

		}
		public ApiException(string message, Exception innerException)
			: base(message, innerException)
		{
		}

		//[SecurityCritical, SecurityTreatAsSafe]
		//[Security.SecurityCritical]
		[SuppressUnmanagedCodeSecurity]
		public static ApiException Win32(string message)
		{
			return ApiException.Win32(message, Marshal.GetLastWin32Error());
		}

		public static ApiException Win32(string message, int errorCode)
		{
			return new ApiException(message, new Win32Exception(errorCode));
		}
	}
}
