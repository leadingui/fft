﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace LeadingUI.Common
{
	public enum EndianType : byte
	{
		[Description("Little")]
		Little = 0,
		[Description("Big")]
		Big = 1
	}
}
