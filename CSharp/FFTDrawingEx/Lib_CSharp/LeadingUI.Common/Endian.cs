﻿using System;
using System.Runtime.InteropServices;

namespace LeadingUI.Common
{
	///
	/// BIG_ENDIAN
	///
	[StructLayout(LayoutKind.Sequential, Pack = 1)]
	public struct BIG_ENDIAN
	{
		public byte Hi;
		public byte Lo;

		public BIG_ENDIAN(byte hi, byte lo)
		{
			Hi = hi;
			Lo = lo;
		}
		
		public ushort Value // little endian value
		{
			get
			{
				return ConCatenating.MakeWord(Lo, Hi);
			}
			set
			{
				Hi = ConCatenating.HiByte(value);
				Lo = ConCatenating.LoByte(value);
			}
		}
	}

	///
	/// LITTLE_ENDIAN
	///
	[StructLayout(LayoutKind.Sequential, Pack = 1)]
	public struct LITTLE_ENDIAN
	{
		public byte Lo;
		public byte Hi;

		public LITTLE_ENDIAN(byte hi, byte lo)
		{
			Hi = hi;
			Lo = lo;
		}
		
		public ushort Value // little endian value
		{
			get
			{
				return ConCatenating.MakeWord(Lo, Hi);
			}
			set
			{
				Hi = ConCatenating.HiByte(value);
				Lo = ConCatenating.LoByte(value);
			}
		}
	}

	public class Endian
	{
		public static short SwapInt16(short v)
		{
			return (short)(((v & 0xff) << 8) | ((v >> 8) & 0xff));
		}

		public static ushort SwapUInt16(ushort v)
		{
			return (ushort)(((v & 0xff) << 8) | ((v >> 8) & 0xff));
		}

		public static int SwapInt32(int v)
		{
			return (int)(((SwapInt16((short)v) & 0xffff) << 0x10) |
						  (SwapInt16((short)(v >> 0x10)) & 0xffff));
		}

		public static uint SwapUInt32(uint v)
		{
			return (uint)(((SwapUInt16((ushort)v) & 0xffff) << 0x10) |
						   (SwapUInt16((ushort)(v >> 0x10)) & 0xffff));
		}

		public static long SwapInt64(long v)
		{
			return (long)(((SwapInt32((int)v) & 0xffffffffL) << 0x20) |
						   (SwapInt32((int)(v >> 0x20)) & 0xffffffffL));
		}

		public static ulong SwapUInt64(ulong v)
		{
			return (ulong)(((SwapUInt32((uint)v) & 0xffffffffL) << 0x20) |
							(SwapUInt32((uint)(v >> 0x20)) & 0xffffffffL));
		}
	}
}
