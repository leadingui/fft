﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LeadingUI.Common
{
	public enum BitMask : byte
	{
		B0 = 0x01,
		B1 = 0x02,
		B2 = 0x04,
		B3 = 0x08,
		B4 = 0x10,
		B5 = 0x20,
		B6 = 0x40,
		B7 = 0x80
	}

	public class BitMasks
	{
		public const byte B0 = 0x01;
		public const byte B1 = 0x02;
		public const byte B2 = 0x04;
		public const byte B3 = 0x08;
		public const byte B4 = 0x10;
		public const byte B5 = 0x20;
		public const byte B6 = 0x40;
		public const byte B7 = 0x80;
	}
}
