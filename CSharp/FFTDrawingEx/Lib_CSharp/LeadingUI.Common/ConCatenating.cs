﻿using System;

namespace LeadingUI.Common
{
    public class ConCatenating
    {
        public static UInt16 MakeWord(Byte bLow, Byte bHigh)
        {
            return (UInt16)(((Byte)(((UInt16)(bLow)) & 0xff)) | ((UInt16)((Byte)(((UInt16)(bHigh)) & 0xff))) << 8);
        }

        public static Int32 MakeLong(UInt16 wLow, UInt16 wHigh)
        {
            return (Int32)(((UInt16)(((Int32)(wLow)) & 0xffff)) | ((Int32)((UInt16)(((Int32)(wHigh)) & 0xffff))) << 16);
        }

        public static UInt16 LoWord(UInt32 l)
        {
            return (UInt16)(((UInt32)(l)) & 0xffff);
        }

        public static UInt16 HiWord(UInt32 l)
        {
            return (UInt16)((((UInt32)(l)) >> 16) & 0xffff);
        }

        public static Byte LoByte(UInt16 w)
        {
            return (Byte)(((UInt16)(w)) & 0xff);
        }

        public static Byte HiByte(UInt16 w)
        {
            return (Byte)((((UInt16)(w)) >> 8) & 0xff);
        }
    }

}
