﻿using System;
using LeadingUI.Helper;
using System.Runtime.InteropServices;

namespace LUI.FourierLib
{
	public class FourierLibWrapper
	{
		private delegate void _Fourier_Init();
		private delegate void _Fourier_UnInit();
		private delegate bool _Fourier_ComplexFFT(float[] data, uint number_of_samples);
		private delegate ulong _Fourier_GetFundamentalFrequency();
		private delegate IntPtr _Fourier_FFTMagnitudes(out int samples);

		private _Fourier_Init _fpInit;
		private _Fourier_UnInit _fpUnInit;
		private _Fourier_ComplexFFT _fpComplexFFT;
		private _Fourier_GetFundamentalFrequency _fpGetFundamentalFrequency;
		private _Fourier_FFTMagnitudes _fpFFTMagnitudes;
		
		private int _hModule = 0;

		public FourierLibWrapper()
		{
			if(InitLibrary())
			{
				_fpInit();
			}
		}

		~FourierLibWrapper()
		{
			UnInitLibrary();
        }

		#region Lib Load/Unload
		private bool InitLibrary()
		{
			if (_hModule != 0)
				NativeMethods.FreeLibrary(_hModule);

			// Dynamic Dll loading
			_hModule = NativeMethods.LoadLibrary("LeadingUI.Algorithm.Fourier.dll");

			if (_hModule == 0)
				return false;

			_fpInit = (_Fourier_Init)Marshal.GetDelegateForFunctionPointer(NativeMethods.GetProcAddress(_hModule, "Fourier_Init"), typeof(_Fourier_Init));
			_fpUnInit = (_Fourier_UnInit)Marshal.GetDelegateForFunctionPointer(NativeMethods.GetProcAddress(_hModule, "Fourier_UnInit"), typeof(_Fourier_UnInit));
			_fpComplexFFT = (_Fourier_ComplexFFT)Marshal.GetDelegateForFunctionPointer(NativeMethods.GetProcAddress(_hModule, "Fourier_ComplexFFT"), typeof(_Fourier_ComplexFFT));
			_fpGetFundamentalFrequency = (_Fourier_GetFundamentalFrequency)Marshal.GetDelegateForFunctionPointer(NativeMethods.GetProcAddress(_hModule, "Fourier_GetFundamentalFrequency"), typeof(_Fourier_GetFundamentalFrequency));
			_fpFFTMagnitudes = (_Fourier_FFTMagnitudes)Marshal.GetDelegateForFunctionPointer(NativeMethods.GetProcAddress(_hModule, "Fourier_FFTMagnitudes"), typeof(_Fourier_FFTMagnitudes));

			if ((_fpComplexFFT == null) || (_fpGetFundamentalFrequency == null) || (_fpFFTMagnitudes == null) ||
				(_fpInit == null) || (_fpUnInit == null))
            {
				if (_hModule != 0)
					NativeMethods.FreeLibrary(_hModule);
				_hModule = 0;
				return false;
			}
			return true;
		}

		private void UnInitLibrary()
		{
			if(_fpUnInit != null)
			{
				_fpUnInit();
            }

			if (_hModule != 0)
			{
				NativeMethods.FreeLibrary(_hModule);
				_hModule = 0;
			}

			_fpInit = null;
			_fpUnInit = null;
			_fpComplexFFT = null;
			_fpGetFundamentalFrequency = null;
			_fpFFTMagnitudes = null;
        }

		#endregion

		public bool ComplexFFT(float[] data, uint number_of_samples)
		{
			if (_fpComplexFFT == null) return false;
			return _fpComplexFFT(data, number_of_samples);
		}

		public UInt64 GetFundamentalFrequency()
		{
			if (_fpGetFundamentalFrequency == null) return 0;
			return _fpGetFundamentalFrequency();
		}

		public bool FFTMagnitudes(out float[] arr)
		{
			if (_fpFFTMagnitudes == null)
			{
				arr = null;
				return false;
			}

			int samples;
			IntPtr ptr = _fpFFTMagnitudes(out samples);
			//samples *= 2;
			arr = new float[samples];

			Marshal.Copy(ptr, arr, 0, samples);
			return true;
		}
    }
}
