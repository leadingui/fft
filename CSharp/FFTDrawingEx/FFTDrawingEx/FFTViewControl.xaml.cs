﻿using System;
using System.Windows.Controls;
using System.ComponentModel;

using OxyPlot;
using OxyPlot.Xps;
using OxyPlot.Axes;
using OxyPlot.Series;

using LUI.FourierLib;

namespace LUI
{
	/// <summary>
	/// Interaction logic for FFTViewControl.xaml
	/// </summary>
	public partial class FFTViewControl : UserControl, INotifyPropertyChanged
	{
		private FourierLibWrapper _fourierWrapper = new FourierLibWrapper();
		public FFTViewControl()
		{
			InitializeComponent();
			
			PlotModelADC = new PlotModel();
			PlotModelFFT = new PlotModel();
			SetUpModel();

		}

		private PlotModel _PlotModelADC;
		public PlotModel PlotModelADC
		{
			get { return _PlotModelADC; }
			set
			{
				_PlotModelADC = value;
				OnPropertyChanged("PlotModelADC");
			}
		}

		private PlotModel _PlotModelFFT;
		public PlotModel PlotModelFFT
		{
			get { return _PlotModelFFT; }
			set
			{
				_PlotModelFFT = value;
				OnPropertyChanged("PlotModelFFT");
			}
		}
		
		private void SetUpModel()
		{
			var dateAxis = new LinearAxis(AxisPosition.Bottom, 0) { MajorGridlineStyle = LineStyle.Solid, MinorGridlineStyle = LineStyle.Dot, Title = "Frame" };
			PlotModelADC.Axes.Add(dateAxis);
			var valueAxis = new LinearAxis(AxisPosition.Left, 0) { MajorGridlineStyle = LineStyle.Solid, MinorGridlineStyle = LineStyle.Dot, Title = "Value" };
			PlotModelADC.Axes.Add(valueAxis);

			dateAxis = new LinearAxis(AxisPosition.Bottom, 0) { MajorGridlineStyle = LineStyle.Solid, MinorGridlineStyle = LineStyle.Dot, Title = "Magnitudes" };
			PlotModelFFT.Axes.Add(dateAxis);
			valueAxis = new LinearAxis(AxisPosition.Left, 0) { MajorGridlineStyle = LineStyle.Solid, MinorGridlineStyle = LineStyle.Dot, Title = "Value" };
			PlotModelFFT.Axes.Add(valueAxis);
		}

		public void LoadData(float[] frames)
		{
			_fourierWrapper.ComplexFFT(frames, (uint)frames.Length);

			float[] vector;
			_fourierWrapper.FFTMagnitudes(out vector);

			LineSeries adcdata = new LineSeries();
			for (int i = 0; i < frames.Length; i++)
			{
				adcdata.Points.Add(new DataPoint(i, frames[i]));
			}
			PlotModelADC.Series.Clear();
			PlotModelADC.Series.Add(adcdata);
			PlotModelADC.InvalidatePlot(true);

			LineSeries fftdata = new LineSeries();
			for (int i = 0; i < vector.Length; i++)
			{
				fftdata.Points.Add(new DataPoint(i, vector[i]));
			}
			PlotModelFFT.Series.Clear();
			PlotModelFFT.Series.Add(fftdata);
			PlotModelFFT.InvalidatePlot(true);
		}
		
		#region INotifyPropertyChanged Members
		public event PropertyChangedEventHandler PropertyChanged;
		protected virtual void OnPropertyChanged(string strPropertyName)
		{
			if (PropertyChanged != null)
			{
				PropertyChanged(this, new PropertyChangedEventArgs(strPropertyName));
			}
		}
		#endregion
	}
}
